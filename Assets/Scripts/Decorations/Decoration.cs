﻿using System;
using UnityEngine;

namespace spaceship
{
	[Serializable]
	internal sealed class Decoration
	{
		[SerializeField]
		private DecorationUI uiPrefab;
		[SerializeField]
		private Vector2 relativePosition;
		[SerializeField]
		private float relativeRollAngle;

		public DecorationUI UIPrefab => uiPrefab;
		public Vector2 RelativePosition => relativePosition;
		public float RelativeRollAngle => relativeRollAngle;

		public Decoration(
			DecorationUI uiPrefab, 
			Vector2 relativePosition, 
			float relativeRollAngle
		)
		{
			this.uiPrefab = uiPrefab;
			this.relativePosition = relativePosition;
			this.relativeRollAngle = relativeRollAngle;
		}
	}
}
