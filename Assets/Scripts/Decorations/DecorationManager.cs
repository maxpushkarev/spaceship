﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace spaceship
{
	internal sealed class DecorationManager : MonoBehaviour
	{
		[SerializeField]
		private UpdateDecorationStatesEvent onUpdateDecorationStatesEvent;
		[SerializeField]
		private UpdateDecorationsEvent onUpdateDecorationsEvent;

		[Inject]
		private IDecorationSettings decorationSettings;
		[Inject]
		private IViewport viewport;

		public void OnViewportUpdated()
		{
			int vpSize = viewport.Size;
			DecorationStates state = vpSize <= decorationSettings.MaxVisibleZoom ? DecorationStates.NORMAL : DecorationStates.HIDDEN;
			onUpdateDecorationStatesEvent.Invoke(state);
			onUpdateDecorationsEvent.Invoke();
		}

		[Serializable]
		private class UpdateDecorationStatesEvent : UnityEvent<DecorationStates>
		{
		}

		[Serializable]
		private class UpdateDecorationsEvent : UnityEvent
		{
		}
	}
}
