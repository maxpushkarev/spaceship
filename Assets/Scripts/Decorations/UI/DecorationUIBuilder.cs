﻿using Zenject;

namespace spaceship
{
	internal sealed class DecorationUIBuilder
	{
		[Inject]
		private DecorationUIFactory decorationUIFactory;
		public DecorationUI SpawnDecorationUI() => decorationUIFactory.Create();
	}
}
