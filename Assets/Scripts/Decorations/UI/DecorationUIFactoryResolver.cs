﻿using System.Collections.Generic;

namespace spaceship
{
	internal sealed class DecorationUIFactoryResolver
	{
		private Dictionary<DecorationUI, DecorationUIBuilder> prefab2BuilderMap;

		public DecorationUIFactoryResolver(Dictionary<DecorationUI, DecorationUIBuilder> prefab2BuilderMap)
		{
			this.prefab2BuilderMap = prefab2BuilderMap;
		}
		public DecorationUI SpawnDecorationUI(DecorationUI decorationUIPrefab) => prefab2BuilderMap[decorationUIPrefab].SpawnDecorationUI();
	}
}
