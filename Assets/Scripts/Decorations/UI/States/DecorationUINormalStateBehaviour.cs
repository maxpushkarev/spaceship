﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class DecorationUINormalStateBehaviour : DecorationUIStateBehaviour
	{
		private ITileSettings tileSettings;
		private IViewport viewport;
		private IZoomSettings zoomSettings;

		[Inject]
		private void Construct(
			ITileSettings tileSettings, 
			IViewport viewport,
			IZoomSettings zoomSettings
		)
		{
			this.tileSettings = tileSettings;
			this.viewport = viewport;
			this.zoomSettings = zoomSettings;
		}

		public override void OnStateEnter()
		{
			viewportContent.EnableDecorations();
		}

		public override void UpdateDecorations()
		{
			viewportContent.HideAllDecorations();

			int viewportSize = viewport.Size;
			SpaceCoords viewportBLCoords = ViewportUtils.GetBottomLeft(viewport, tileSettings);
			Position<long> worldBlVp = viewportBLCoords.worldCoords;

			Vector3 decorationScale = ViewportContentUtils.CalculateScale(zoomSettings, viewport.Size);
			Vector2 cellUISize = ViewportContentUtils.CalculateCellUISize(viewport, viewportContent);

			for (int i = 0; i < viewportSize; i++)
			{
				for (int j = 0; j < viewportSize; j++)
				{
					SpaceCoords coords = new SpaceCoords(
						viewportBLCoords.worldCoords.x + j,
						viewportBLCoords.worldCoords.y + i,
						tileSettings
					);

					Tile tile;
					Cell cell;
					tileSettings.GetTileAndCell(coords, out tile, out cell);

					Position<long> cellWorldPos = coords.worldCoords;
					Vector2 cellAnchoredPos = ViewportContentUtils.CalculateCellAnchoredPosition(cellWorldPos, worldBlVp, cellUISize);

					Decoration[] cellDecorations = cell.Decorations;
					for(int d = 0; d < cellDecorations.Length; d++)
					{
						Decoration decoration = cellDecorations[d];
						Vector2 anchoredDecorationPos = cellAnchoredPos + decoration.RelativePosition * cellUISize;
						float rollAngle = decoration.RelativeRollAngle;
						DecorationUI decorationInstance = viewportContent.SpawnDecoration(decoration.UIPrefab);
						RectTransform decorationInstanceRectTransform = decorationInstance.ViewportContentItem.RectTransform;
						decorationInstanceRectTransform.anchoredPosition = anchoredDecorationPos;
						decorationInstanceRectTransform.localEulerAngles = new Vector3(0.0f, 0.0f, rollAngle);
						decorationInstanceRectTransform.localScale = decorationScale;
					}
				}
			}
		}
	}
}
