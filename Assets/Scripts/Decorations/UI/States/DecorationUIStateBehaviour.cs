﻿using Zenject;

namespace spaceship
{
	internal abstract class DecorationUIStateBehaviour : BaseStateBehaviour
	{
		protected ViewportContent viewportContent;

		[Inject]
		private void Construct(ViewportContent viewportContent)
		{
			this.viewportContent = viewportContent;
		}

		public abstract void UpdateDecorations();
	}
}
