﻿using System;

namespace spaceship
{
	[Serializable]
	internal sealed class DecorationUIStatesDescription : StatesDescription<DecorationStates, DecorationUIStateBehaviour, DecorationUIStateConfiguration>
	{
		public override DecorationUIStateConfiguration this[DecorationStates state] => stateConfigurations[(int)state];
	}
}
