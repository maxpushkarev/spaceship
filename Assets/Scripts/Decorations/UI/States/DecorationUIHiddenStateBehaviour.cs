﻿namespace spaceship
{
	internal sealed class DecorationUIHiddenStateBehaviour : DecorationUIStateBehaviour
	{
		public override void OnStateEnter()
		{
			viewportContent.DisableDecorations();
		}

		public override void UpdateDecorations()
		{
		}
	}
}
