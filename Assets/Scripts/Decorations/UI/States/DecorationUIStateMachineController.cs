﻿using System;

namespace spaceship
{
	internal sealed class DecorationUIStateMachineController : StateMachineController<DecorationStates, DecorationUIStateBehaviour,
																	DecorationUIStateConfiguration, DecorationUIStatesDescription>
	{
		private Action<DecorationUIStateBehaviour> updateDecorationsDelegateInstance;

		private void Awake()
		{
			updateDecorationsDelegateInstance = new Action<DecorationUIStateBehaviour>(UpdateDecorations);
		}

		public void UpdateDecorations()
		{
			StatesDescription[CurrentState].ForEach(updateDecorationsDelegateInstance);
		}

		public void UpdateDecorations(DecorationUIStateBehaviour decorationUIStateBehaviour)
		{
			decorationUIStateBehaviour.UpdateDecorations();
		}
	}
}
