﻿using UnityEngine;

namespace spaceship
{
	internal sealed class DecorationUI : BaseViewportContentPoolableUI
	{
		[SerializeField]
		private ViewportContentItem viewportContentItem;
		public ViewportContentItem ViewportContentItem => viewportContentItem;
	}
}
