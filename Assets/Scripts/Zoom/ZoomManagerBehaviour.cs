﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace spaceship
{
	internal sealed class ZoomManagerBehaviour : MonoBehaviour, IInitializable
	{
		[SerializeField]
		private UpdateZoomEvent onZoomUpdateEvent;
		[SerializeField]
		private UpdateZoomStateEvent onZoomStateChangedEvent;

		private IZoomSettings zoomSettings;
		private int zoomValue;

		[Inject]
		private void Construct(IZoomSettings zoomSettings) => this.zoomSettings = zoomSettings;
		[Inject]
		public void Initialize() => zoomValue = zoomSettings.MinScale;

		private void Awake()
		{
			onZoomUpdateEvent.Invoke(zoomValue);
		}

		public void OnZoomInputEvent(int zoomInputDelta)
		{
			int newZoom = Mathf.Max(
				zoomSettings.MinScale,
				Mathf.Min(zoomSettings.MaxScale, zoomValue + zoomInputDelta)
			);

			if (newZoom != zoomValue)
			{
				zoomValue = newZoom;
				ZoomStates zoomState = newZoom < zoomSettings.ModeBorderScale ? ZoomStates.NORMAL : ZoomStates.HIGH_SCALED;
				onZoomStateChangedEvent.Invoke(zoomState);
				onZoomUpdateEvent.Invoke(zoomValue);
			}
		}

		[Serializable]
		private class UpdateZoomEvent : UnityEvent<int>
		{
		}

		[Serializable]
		private class UpdateZoomStateEvent : UnityEvent<ZoomStates>
		{
		}
	}
}
