﻿using UnityEngine;
using UnityEngine.UI;

namespace spaceship
{
	internal sealed class ZoomUIBehaviour : MonoBehaviour
	{
		[SerializeField]
		private Text zoomText;
		[SerializeField]
		private string zoomTextFormat = "Zoom : {0}";

		public void OnZoomValueChanged(int value)
		{
			zoomText.text = string.Format(zoomTextFormat, value);
		}
	}
}
