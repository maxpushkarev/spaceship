﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class ServiceInjector : MonoBehaviour
	{
		[Inject]
		private void Construct(
			SceneContext sceneContext, 
			IInputBindings inputBindings,
			IPlanetSettings planetSettings,
			ITileSettings tileSettings
		)
		{
			DiContainer container = sceneContext.Container;
			container.Inject(inputBindings);
			container.Inject(planetSettings);
			container.Inject(tileSettings);
		}
	}
}
