﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class PlayerBehaviour : MonoBehaviour, IPlayer
	{
		[SerializeField]
		private float rating;
		public float Rating => rating;

		[Inject]
		private void Construct(IPlayerSettings playerSettings)
		{
			rating = Mathf.RoundToInt(Random.Range(playerSettings.MinRating, playerSettings.MaxRating));
		}
	}
}
