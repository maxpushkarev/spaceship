﻿namespace spaceship
{
	internal interface IPlayer
	{
		float Rating { get; }
	}
}
