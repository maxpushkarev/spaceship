﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class PlayerInstaller : MonoInstaller
	{
		[SerializeField]
		private PlayerBehaviour playerBehaviour;

		public override void InstallBindings()
		{
			Container.BindInstance<IPlayer>(playerBehaviour).NonLazy();
		}
	}
}
