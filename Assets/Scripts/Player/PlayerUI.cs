﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace spaceship
{
	internal sealed class PlayerUI : MonoBehaviour
	{
		[SerializeField]
		private Text playerText;
		[SerializeField]
		private string playerTextFormat = "Player rating: {0}";

		private IPlayer player;

		[Inject]
		private void Construct(IPlayer player) => playerText.text = string.Format(playerTextFormat, player.Rating);
	}
}
