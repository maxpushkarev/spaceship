﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace spaceship
{
	internal sealed class ShipUI : MonoBehaviour
	{
		[SerializeField]
		private Text shipText;
		[SerializeField]
		private string shipTextFormat = "Ship coords: [x:{0}; y:{1}]";
		private IViewport viewport;

		[Inject]
		private void Construct(IViewport viewport)
		{
			this.viewport = viewport;
		}

		public void OnViewportUpdated()
		{
			SpaceCoords viewportCenter = viewport.Center;
			Position<long> worldCoords = viewportCenter.worldCoords;
			shipText.text = string.Format(shipTextFormat, worldCoords.x.ToString(), worldCoords.y.ToString());
		}
	}
}
