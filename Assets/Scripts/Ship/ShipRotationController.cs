﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class ShipRotationController : MonoBehaviour
	{
		private ViewportContent viewportContent;

		[Inject]
		private void Construct(ViewportContent viewportContent) => this.viewportContent = viewportContent;

		public void OnShipMoved(SpaceCoords direction)
		{
			viewportContent.SpaceshipItem.transform.rotation = Quaternion.LookRotation(
				Vector3.forward,
				new Vector3(
					direction.worldCoords.x,
					direction.worldCoords.y,
					0
				)
			);
		}
	}
}
