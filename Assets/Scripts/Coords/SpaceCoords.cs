﻿using System;
using System.Runtime.CompilerServices;
using Unity.IL2CPP.CompilerServices;

namespace spaceship
{
	internal struct SpaceCoords : IEquatable<SpaceCoords>
	{
		public static SpaceCoords ZERO = new SpaceCoords();

		private ITileSettings tileSettings;
		public readonly Position<long> worldCoords;
		public readonly Position<long> clusterCoords;
		public readonly Position<int> tileCoords;
		public readonly Position<int> cellCoords;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private SpaceCoords(Position<long> worldCoords, ITileSettings tileSettings) : this()
		{
			this.worldCoords = worldCoords;
			this.tileSettings = tileSettings;

			long clusterCellSize = SpaceUtils.GetClusterSizeInCells(tileSettings);

			this.clusterCoords = new Position<long>(
				GetUnrepeatedCoord(worldCoords.x, clusterCellSize), 
				GetUnrepeatedCoord(worldCoords.y, clusterCellSize)
			);
			this.tileCoords = new Position<int>(
				GetTileCoord(worldCoords.x),
				GetTileCoord(worldCoords.y)
			);
			this.cellCoords = new Position<int>(
				GetCellCoord(worldCoords.x),
				GetCellCoord(worldCoords.y)
			);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Il2CppSetOption(Option.DivideByZeroChecks, false)]
		private long GetUnrepeatedCoord(long input, long chunkSize)
		{
			int sign = GetLongSign(input);
			long posCoord = input / chunkSize;
			long negCoord = ((input + 1) / chunkSize) - 1;
			return posCoord * (1 - sign) + negCoord * sign;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private int GetLongSign(long input)
		{
			unchecked
			{
				//0 if non-negative, 1 otherwise
				return (int)(
					((ulong)input) >> 63
				);
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Il2CppSetOption(Option.NullChecks, false)]
		private int GetCellCoord(long input)
		{
			return GetSubIndexWithinChunk(input, tileSettings.TileSize);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private int GetSubIndexWithinChunk(long unrepeatedCoord, int chunkSize)
		{
			long posCoord = unrepeatedCoord % chunkSize;
			long negCoord = (chunkSize - 1) + ((unrepeatedCoord + 1) % chunkSize);
			long sign = GetLongSign(unrepeatedCoord);
			return (int)(posCoord * (1 - sign) + negCoord * sign);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Il2CppSetOption(Option.NullChecks, false)]
		private int GetTileCoord(long input)
		{
			long unrepeatedCoord = GetUnrepeatedCoord(input, tileSettings.TileSize);
			int clusterSize = tileSettings.ClusterSize;
			return GetSubIndexWithinChunk(unrepeatedCoord, clusterSize);
		}

		public SpaceCoords(long x, long y, ITileSettings tileSettings) : this(new Position<long>(x,y), tileSettings)
		{
		}

		public override string ToString() => $"World: [{worldCoords}]; Tile: [{tileCoords}]; Cell: [{cellCoords}]";
		public bool Equals(SpaceCoords otherCoords) => worldCoords.Equals(otherCoords.worldCoords);
		public override bool Equals(object obj) => Equals((SpaceCoords)obj);
		public override int GetHashCode() => worldCoords.GetHashCode();
		public static bool operator ==(SpaceCoords l, SpaceCoords r) => l.Equals(r);
		public static bool operator !=(SpaceCoords l, SpaceCoords r) => !(l.Equals(r));
		public static SpaceCoords operator +(SpaceCoords l, SpaceCoords r) => new SpaceCoords(l.worldCoords.x + r.worldCoords.x, l.worldCoords.y + r.worldCoords.y, l.tileSettings);
		public static SpaceCoords operator -(SpaceCoords l, SpaceCoords r) => new SpaceCoords(l.worldCoords.x - r.worldCoords.x, l.worldCoords.y - r.worldCoords.y, l.tileSettings);
	}
}
