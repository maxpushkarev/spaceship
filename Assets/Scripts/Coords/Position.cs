﻿using System;

namespace spaceship
{
	internal struct Position<T> : IEquatable<Position<T>> where T : struct, IEquatable<T>
	{
		public readonly T x;
		public readonly T y;

		public Position(T x, T y)
		{
			this.x = x;
			this.y = y;
		}

		public bool Equals(Position<T> other) => (x.Equals(other.x)) && (y.Equals(other.y));
		public override string ToString() => $"X = {x}; Y = {y}";
		public override bool Equals(object obj) => Equals((Position<T>)obj);
		public override int GetHashCode() => (x.GetHashCode() + y.GetHashCode()) % 113;

		public static bool operator ==(Position<T> l, Position<T> r) => l.Equals(r);
		public static bool operator !=(Position<T> l, Position<T> r) => !(l.Equals(r));
	}

}
