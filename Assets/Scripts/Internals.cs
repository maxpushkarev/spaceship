﻿#if UNITY_EDITOR
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("SpaceshipEditor"), InternalsVisibleTo("SpaceshipTests")]
#endif
