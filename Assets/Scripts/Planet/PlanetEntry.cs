﻿namespace spaceship
{
	internal struct PlanetEntry
	{
		public readonly Planet planet;
		public readonly Position<long> worldPos;

		public PlanetEntry(Planet planet, Position<long> blCluster)
		{
			this.planet = planet;
			this.worldPos = new Position<long>(
				blCluster.x + planet.clusterPos.x,
				blCluster.y + planet.clusterPos.y
			);
		}
	}
}
