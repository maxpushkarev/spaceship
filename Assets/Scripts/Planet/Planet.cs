﻿using UnityEngine;

namespace spaceship
{
	internal sealed class Planet
	{
		public readonly float ratingDiff;
		public readonly int rating;
		public readonly Position<long> clusterPos;

		public Planet()
		{
		}

		public Planet(int rating, SpaceCoords coords, IPlayer player)
		{
			this.rating = rating;
			this.ratingDiff = Mathf.Abs(rating - player.Rating);
			this.clusterPos = coords.worldCoords;
		}

		public static bool operator true(Planet planetData) => planetData != null;
		public static bool operator false(Planet planetData) => planetData == null;
	}
}
