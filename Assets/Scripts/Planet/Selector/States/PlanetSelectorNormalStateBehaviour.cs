﻿using System.Collections.Generic;
using Zenject;

namespace spaceship
{
	internal sealed class PlanetSelectorNormalStateBehaviour : PlanetSelectorStateBehaviour
	{
		private IViewport viewport;
		[Inject]
		private void Construct(IViewport viewport) => this.viewport = viewport;

		public override List<PlanetEntry> SelectPlanets() => planetSelector.SelectPlayerRelevantPlanets(viewport.Size * viewport.Size);
	}
}
