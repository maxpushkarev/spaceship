﻿using System;

namespace spaceship
{
	[Serializable]
	internal sealed class PlanetSelectorStatesDescription : StatesDescription<ZoomStates, PlanetSelectorStateBehaviour, PlanetSelectorStateConfiguration>
	{
		public override PlanetSelectorStateConfiguration this[ZoomStates state] => stateConfigurations[(int)state];
	}
}
