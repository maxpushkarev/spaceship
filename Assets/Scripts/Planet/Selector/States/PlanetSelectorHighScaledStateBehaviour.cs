﻿using System.Collections.Generic;

namespace spaceship
{
	internal sealed class PlanetSelectorHighScaledStateBehaviour : PlanetSelectorStateBehaviour
	{
		public override List<PlanetEntry> SelectPlanets() => planetSelector.SelectPlayerRelevantPlanets(planetSettings.NearestByRatingPlanetCount);
	}
}
