﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal abstract class PlanetSelectorStateBehaviour : BaseStateBehaviour
	{
		[SerializeField]
		protected PlanetSelector planetSelector;
		protected IPlanetSettings planetSettings;

		[Inject]
		private void Construct(IPlanetSettings planetSettings) => this.planetSettings = planetSettings;
		public abstract List<PlanetEntry> SelectPlanets();
	}
}
