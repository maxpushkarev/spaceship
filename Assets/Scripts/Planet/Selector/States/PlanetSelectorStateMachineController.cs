﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace spaceship
{
	internal sealed class PlanetSelectorStateMachineController : StateMachineController<ZoomStates, PlanetSelectorStateBehaviour,
																	PlanetSelectorStateConfiguration, PlanetSelectorStatesDescription>
	{
		[SerializeField]
		private PlanetsSelectedEvent onPlanetsSelected;
		private Action<PlanetSelectorStateBehaviour> selectPlanetsDelegateInstance;

		private void Awake()
		{
			selectPlanetsDelegateInstance = SelectPlanets;
		}

		public void OnViewportUpdate() => StatesDescription[CurrentState].ForEach(selectPlanetsDelegateInstance);

		private void SelectPlanets(PlanetSelectorStateBehaviour planetSelectorStateBehaviour)
		{
			onPlanetsSelected.Invoke(planetSelectorStateBehaviour.SelectPlanets());
		}

		[Serializable]
		private class PlanetsSelectedEvent : UnityEvent<List<PlanetEntry>>
		{
		}

	}
}
