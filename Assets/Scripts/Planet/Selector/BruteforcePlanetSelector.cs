﻿using System.Collections.Generic;
using Zenject;

namespace spaceship
{
	internal sealed class BruteforcePlanetSelector : PlanetSelector
	{
		private List<PlanetEntry> generalPlanetsCollection;

		[Inject]
		public override void Initialize()
		{
			generalPlanetsCollection = new List<PlanetEntry>(cachedPlanetInfoList.Capacity);
		}

		public override List<PlanetEntry> SelectPlayerRelevantPlanets(int count2Select)
		{
			generalPlanetsCollection.Clear();
			cachedPlanetInfoList.Clear();

			long clusterSizeInCells = SpaceUtils.GetClusterSizeInCells(tileSettings);
			int viewportSize = viewport.Size;
			SpaceCoords baseCoords = ViewportUtils.GetBottomLeft(viewport, tileSettings);

			for(int i = 0; i < viewportSize; i++)
			{
				for (int j = 0; j < viewportSize; j++)
				{
					SpaceCoords coords = new SpaceCoords(
						baseCoords.worldCoords.x + j,
						baseCoords.worldCoords.y + i,
						tileSettings
					);

					Tile tile;
					Cell cell;
					Planet planet;

					tileSettings.GetTileAndCell(coords, out tile, out cell);
					planet = cell.Planet;

					if(planet)
					{
						generalPlanetsCollection.Add(new PlanetEntry(
							planet,
							new Position<long>(
								coords.clusterCoords.x * clusterSizeInCells,
								coords.clusterCoords.y * clusterSizeInCells
							)
						));
					}
				}
			}

			int counter = 0;
			while(cachedPlanetInfoList.Count < count2Select)
			{
				if(counter == generalPlanetsCollection.Count)
				{
					break;
				}

				for (int i = counter; i < generalPlanetsCollection.Count; i++)
				{
					PlanetEntry cur = generalPlanetsCollection[i];
					if (cur.planet.ratingDiff < generalPlanetsCollection[counter].planet.ratingDiff)
					{
						PlanetEntry tmp = generalPlanetsCollection[counter];
						generalPlanetsCollection[counter] = cur;
						generalPlanetsCollection[i] = tmp;
					}
				}

				cachedPlanetInfoList.Add(generalPlanetsCollection[counter]);
				counter++;
			}

			return cachedPlanetInfoList;
		}
	}
}
