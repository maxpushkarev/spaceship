﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Zenject;
using Debug = UnityEngine.Debug;

namespace spaceship
{
	[Il2CppSetOption(Option.NullChecks, false)]
	[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
	[Il2CppSetOption(Option.DivideByZeroChecks, false)]
	internal sealed class AcceleratedPlanetSelector : PlanetSelector
	{
		[SerializeField]
		private int nodeCapacity;
		[SerializeField]
		private int leafSize = 4;

		private Context context;
		private IPlanetSettings planetSettings;
		private QTree qTree;

		private Dictionary<QTreeNode, int> node2EntryIndexMap;
		private List<QTreeNodeEntries> nodeInClusterEntries;
		private List<int> nodeSortedListOffsets;
		private int resultCapacity;

		[Inject]
		private void Construct(Context context, IPlanetSettings planetSettings)
		{
			this.context = context;
			this.planetSettings = planetSettings;
		}

		[Inject]
		public override void Initialize() {
			node2EntryIndexMap = new Dictionary<QTreeNode, int>();
			nodeInClusterEntries = new List<QTreeNodeEntries>();
			nodeSortedListOffsets = new List<int>();
			resultCapacity = Mathf.Max(nodeCapacity, planetSettings.NearestByRatingPlanetCount);
			qTree = new QTree(resultCapacity, leafSize);
			context.Container.Inject(qTree);
		}

		private void OnDestroy()
		{
			qTree = null;
			GC.Collect();
		}

		public override List<PlanetEntry> SelectPlayerRelevantPlanets(int count2Select)
		{
			cachedPlanetInfoList.Clear();
			nodeInClusterEntries.Clear();
			node2EntryIndexMap.Clear();
			nodeSortedListOffsets.Clear();

			SpaceCoords blViewport;
			SpaceCoords trViewport;
			GetViewportBlAndTr(out blViewport, out trViewport);

			Position<long> blClusterIndices = blViewport.clusterCoords;
			Position<long> trClusterIndices = trViewport.clusterCoords;

			long size = SpaceUtils.GetClusterSizeInCells(tileSettings);
			
			for (long i = blClusterIndices.x; i <= trClusterIndices.x; i++)
			{
				for (long j = blClusterIndices.y; j <= trClusterIndices.y; j++)
				{
					Position<long> blCluster = new Position<long>(i * size, j * size);
					CollectBatches(blCluster, blViewport, trViewport);
				}
			}

			CollectPlanetsFromBatches(count2Select);
			ReleaseSpawnedLists();
			return cachedPlanetInfoList;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private void GetViewportBlAndTr(out SpaceCoords bl, out SpaceCoords tr)
		{
			int vpSizeMinusOne = viewport.Size - 1;
			bl = ViewportUtils.GetBottomLeft(viewport, tileSettings);
			Position<long> blViewportWorld = bl.worldCoords;
			tr = new SpaceCoords(
				blViewportWorld.x + vpSizeMinusOne,
				blViewportWorld.y + vpSizeMinusOne,
				tileSettings
			);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private void CollectPlanetsFromBatches(int count2Select)
		{
			SpaceCoords blViewport;
			SpaceCoords trViewport;
			GetViewportBlAndTr(out blViewport, out trViewport);
			Position<long> worldVpBl = blViewport.worldCoords;
			Position<long> worldVpTr = trViewport.worldCoords;

			int currentEntryIndex = -1;

			while(true)
			{
				currentEntryIndex = FindEntryIndex();

				//not found planets anymore
				if(currentEntryIndex < 0)
				{
					break;
				}

				int offset = nodeSortedListOffsets[currentEntryIndex];
				//planetdata will not be evaluated anymore
				nodeSortedListOffsets[currentEntryIndex]++;

				QTreeNodeEntries nodeEntries = nodeInClusterEntries[currentEntryIndex];
				QTreeNode node = nodeEntries.node;
				List<Planet> sortedNodePlanets = node.sortedPlanetInfos;
				Planet planetData = sortedNodePlanets[offset];
				List<Position<long>> clusterEntries = nodeEntries.clusterEntries;
				int clusterEntriesCount = clusterEntries.Count;

				for (int i = 0; i<clusterEntriesCount; i++)
				{
					Position<long> blCluster = clusterEntries[i];
					PlanetEntry candidate = new PlanetEntry(planetData, blCluster);

					//check planet within viewport
					if(PlanetWithinViewport(candidate, worldVpBl, worldVpTr))
					{
						if(cachedPlanetInfoList.Count < count2Select)
						{
							cachedPlanetInfoList.Add(candidate);
						}
						else
						{
							break;
						}
					}
				}

				if (cachedPlanetInfoList.Count == count2Select)
				{
					break;
				}
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private bool PlanetWithinViewport(
			PlanetEntry candidate, 
			Position<long> worldVpBl,
			Position<long> worldVpTr
		)
		{
			Position<long> candidateWorld = candidate.worldPos;
			return (candidateWorld.x >= worldVpBl.x) &&
				(candidateWorld.y >= worldVpBl.y) &&
				 (candidateWorld.x <= worldVpTr.x) &&
				(candidateWorld.y <= worldVpTr.y);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private int FindEntryIndex()
		{
			int index = -1;
			float min = float.PositiveInfinity;

			int nodeEntriesCount = nodeInClusterEntries.Count;
			for (int i = 0; i < nodeEntriesCount; i++)
			{
				int offset = nodeSortedListOffsets[i];
				QTreeNode node = nodeInClusterEntries[i].node;
				List<Planet> sortedNodePlanets = node.sortedPlanetInfos;
				if(offset < sortedNodePlanets.Count)
				{
					float currentRatingDistance = sortedNodePlanets[offset].ratingDiff;

					if (index < 0)
					{
						//first found candidate
						index = i;
						min = currentRatingDistance;
					}
					else
					{
						//check others
						if(currentRatingDistance < min)
						{
							index = i;
							min = currentRatingDistance;
						}
					}
				}
			}

			return index;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private void ReleaseSpawnedLists()
		{
			int nodeEntriesCount = nodeInClusterEntries.Count;
			ListPool<Position<long>> pool = ListPool<Position<long>>.Instance;

			for (int i = 0; i<nodeEntriesCount; i++)
			{
				pool.Despawn(nodeInClusterEntries[i].clusterEntries);
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private void CollectBatches(
			Position<long> blCluster, 
			SpaceCoords blViewport, 
			SpaceCoords trViewport
		)
		{
			CollectBatches(blCluster, qTree.RootNode, blViewport, trViewport);
		}

		private void CollectBatches(
			Position<long> blCluster, 
			QTreeNode node,
			SpaceCoords blViewport,
			SpaceCoords trViewport
		)
		{
			Position<long> relativeNodeBl = node.relativeBlCoords;
			long nodeSizeMinusOne = node.size - 1;
			Position<long> worldNodeBl = new Position<long>(
				blCluster.x + relativeNodeBl.x, 
				blCluster.y + relativeNodeBl.y
			);
			Position<long> worldNodeTr = new Position<long>(
				worldNodeBl.x + nodeSizeMinusOne,
				worldNodeBl.y + nodeSizeMinusOne
			);

			Position<long> worldVpBl = blViewport.worldCoords;
			Position<long> worldVpTr = trViewport.worldCoords;

			if(
				(worldNodeBl.x >= worldVpBl.x) &&
				(worldNodeBl.y >= worldVpBl.y) &&
				(worldNodeTr.x <= worldVpTr.x) &&
				(worldNodeTr.y <= worldVpTr.y)
			)
			{
				//full node within viewport;
				AddNodeInMap(node, blCluster);
				return;
			}

			if(worldNodeTr.x < worldVpBl.x)
			{
				//node fully culled
				return;
			}

			if(worldNodeBl.x > worldVpTr.x)
			{
				//node fully culled
				return;
			}

			if(worldNodeTr.y < worldVpBl.y)
			{
				//node fully culled
				return;
			}

			if (worldNodeBl.y > worldVpTr.y)
			{
				//node fully culled
				return;
			}

			if (node.IsLeaf)
			{
				//partially culled leaf
				AddNodeInMap(node, blCluster);
				return;
			}

			//non-leaf node partially culled
			//check childs
			CollectBatches(blCluster, node.Bl, blViewport, trViewport);
			CollectBatches(blCluster, node.Br, blViewport, trViewport);
			CollectBatches(blCluster, node.Tl, blViewport, trViewport);
			CollectBatches(blCluster, node.Tr, blViewport, trViewport);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private void AddNodeInMap(QTreeNode node, Position<long> blCluster)
		{
			int nodeEntryIndex = -1;
			if(!node2EntryIndexMap.TryGetValue(node, out nodeEntryIndex))
			{
				QTreeNodeEntries newEntries = new QTreeNodeEntries(node);
				newEntries.clusterEntries.Add(blCluster);
				nodeInClusterEntries.Add(newEntries);
				nodeSortedListOffsets.Add(0);
				node2EntryIndexMap.Add(node, nodeInClusterEntries.Count - 1);
				return;
			}

			nodeInClusterEntries[nodeEntryIndex].clusterEntries.Add(blCluster);
		}

		private sealed class QTree : IInitializable
		{
			[Inject]
			private ITileSettings tileSettings;
			[Inject]
			private IPlanetSettings planetSettings;

			private int capacity;
			private int leafSize;
			public QTreeNode RootNode { get; private set; }

			public QTree(int capacity, int leafSize)
			{
				this.capacity = capacity;
				this.leafSize = leafSize;
			}

			[Inject]
			public void Initialize()
			{
				Stopwatch stopWatch = new Stopwatch();
				stopWatch.Start();
				RootNode = new QTreeNode(
					new Position<long>(0, 0),
					SpaceUtils.GetClusterSizeInCells(tileSettings),
					capacity,
					tileSettings,
					planetSettings
				);
				RootNode.Build(leafSize);
				stopWatch.Stop();
				TimeSpan ts = stopWatch.Elapsed;
				string elapsedTime = String.Format(
					"{0:00}:{1:00}:{2:00}.{3:00}",
					ts.Hours, ts.Minutes, ts.Seconds,
					ts.Milliseconds / 10
				);
				Debug.Log($"<color=blue>Qtree built. Building time: [{elapsedTime}]</color>");
			}
		}

		private struct QTreeNodeEntries : IEquatable<QTreeNodeEntries>
		{
			public readonly QTreeNode node;
			public readonly List<Position<long>> clusterEntries;

			public QTreeNodeEntries(QTreeNode node)
			{
				this.node = node;
				this.clusterEntries = ListPool<Position<long>>.Instance.Spawn();
			}

			public override int GetHashCode() => node.GetHashCode();
			public bool Equals(QTreeNodeEntries another) => node.Equals(another.node);
			public override bool Equals(object obj) => Equals((QTreeNodeEntries)obj);
		}

		private sealed class QTreeNode : IEquatable<QTreeNode>
		{
			private static long ID_COUNTER = 0;

			private ITileSettings tileSettings;
			private IPlanetSettings planetSettings;

			public QTreeNode Bl { get; private set; }
			public QTreeNode Br { get; private set; }
			public QTreeNode Tl { get; private set; }
			public QTreeNode Tr { get; private set; }
			public bool IsLeaf => Bl == null;

			private readonly long id;
			public List<Planet> sortedPlanetInfos;
			public readonly Position<long> relativeBlCoords;
			public readonly int capacity;
			public readonly long size;

			public QTreeNode(
				Position<long> blCoords, 
				long size,
				int capacity,
				ITileSettings tileSettings,
				IPlanetSettings planetSettings
			)
			{
				this.tileSettings = tileSettings;
				this.planetSettings = planetSettings;

				this.relativeBlCoords = blCoords;
				this.size = size;
				this.capacity = capacity;
				this.sortedPlanetInfos = new List<Planet>();
				this.id = ID_COUNTER++;
			}

			public override int GetHashCode()
			{
				return id.GetHashCode();
			}

			public bool Equals(QTreeNode another)
			{
				return id.Equals(another.id);
			}

			public override bool Equals(object obj)
			{
				return Equals((QTreeNode)obj);
			}

			public void Build(int leafSize)
			{
				long x = relativeBlCoords.x;
				long y = relativeBlCoords.y;

				if (size == leafSize)
				{
					FillUnorderedList();
					sortedPlanetInfos.Sort((pd1, pd2) => pd1.ratingDiff.CompareTo(pd2.ratingDiff));
					return;
				}

				long newSize = size / 2;
				
				Bl = new QTreeNode(
					relativeBlCoords, newSize, capacity, tileSettings, planetSettings
				);
				Br = new QTreeNode(
					new Position<long>(x + newSize, y), newSize, capacity, tileSettings, planetSettings
				);
				Tl = new QTreeNode(
					new Position<long>(x, y + newSize), newSize, capacity, tileSettings, planetSettings
				);
				Tr = new QTreeNode(
					new Position<long>(x + newSize, y + newSize), newSize, capacity, tileSettings, planetSettings
				);

				Bl.Build(leafSize);
				Br.Build(leafSize);
				Tl.Build(leafSize);
				Tr.Build(leafSize);

				CollectSortedListFromSubnodes();
			}

			private void CollectSortedListFromSubnodes()
			{
				QTreeNode[] nodes = new QTreeNode[] { Bl, Br, Tl, Tr };
				int[] indices = new int[] { 0, 0, 0, 0};
				while(
					(sortedPlanetInfos.Count < capacity)
				)
				{
					int index = FindIndexOfMinPlanet(nodes, indices);
					if(index < 0)
					{
						break;
					}
					sortedPlanetInfos.Add(nodes[index].sortedPlanetInfos[indices[index]]);
					indices[index]++;
				}
			}

			private int FindIndexOfMinPlanet(QTreeNode[] nodes, int[] indices)
			{
				int index = -1;
				for(int i = 0; i < 4; i++)
				{
					if(indices[i] >= nodes[i].sortedPlanetInfos.Count)
					{
						continue;
					}
					
					if(index < 0)
					{
						//first
						index = i;
					}
					else
					{
						if (
							nodes[i].sortedPlanetInfos[indices[i]].ratingDiff < nodes[index].sortedPlanetInfos[indices[index]].ratingDiff
						)
						{
							index = i;
						}
					}
				}

				return index;
			}

			private void FillUnorderedList()
			{
				long x = relativeBlCoords.x;
				long y = relativeBlCoords.y;

				for (int i = 0; i < size; i++)
				{
					for (int j = 0; j < size; j++)
					{
						SpaceCoords coords = new SpaceCoords(
							x + j,
							y + i,
							tileSettings
						);

						Tile tile;
						Cell cell;
						tileSettings.GetTileAndCell(coords, out tile, out cell);
						if (cell.Planet)
						{
							sortedPlanetInfos.Add(cell.Planet);
						}
					}
				}
			}
		}
	}
}
