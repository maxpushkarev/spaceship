﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal abstract class PlanetSelector : MonoBehaviour, IInitializable
	{
		protected IPlayer player;
		protected ITileSettings tileSettings;
		protected IViewport viewport;
		protected List<PlanetEntry> cachedPlanetInfoList;

		[Inject]
		private void Construct(
			IPlayer player,
			ITileSettings tileSettings,
			IViewport viewport,
			IPlanetSettings planetSettings, 
			IZoomSettings zoomSettings
		)
		{
			this.player = player;
			this.tileSettings = tileSettings;
			this.viewport = viewport;

			int lastZoomSizeInNormalMode = zoomSettings.ModeBorderScale - 1;
			int maxPossiblePlanetCount = Mathf.Max(lastZoomSizeInNormalMode * lastZoomSizeInNormalMode, planetSettings.NearestByRatingPlanetCount);
			this.cachedPlanetInfoList = new List<PlanetEntry>(maxPossiblePlanetCount);
		}

		public abstract void Initialize();
		public abstract List<PlanetEntry> SelectPlayerRelevantPlanets(int count);
	}
}
