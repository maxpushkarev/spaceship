﻿using UnityEngine;
using UnityEngine.UI;

namespace spaceship
{
	internal sealed class PlanetRatingUI : BaseViewportContentPoolableUI
	{
		[SerializeField]
		private Text textUI;

		public void Prepare(string value)
		{
			textUI.text = value;
			textUI.enabled = true;
			gameObject.SetActive(true);
		}

		public override void Hide()
		{
			gameObject.SetActive(false);
			textUI.enabled = false;
		}
	}
}
