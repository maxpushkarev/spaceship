﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class PlanetUI : BaseViewportContentPoolableUI
	{
		[SerializeField]
		private ViewportContentItem item;
		public ViewportContentItem Item => item;
		
		[Inject]
		private ViewportContent viewportContent;

		private int nextAccessibleRatingUIIndex;
		private List<PlanetRatingUI> ratingUIList;

		private void Awake()
		{
			ratingUIList = new List<PlanetRatingUI>();
		}

		public PlanetRatingUI CreatePlanetRatingUI()
		{
			PlanetRatingUI planetRatingUI = null;

			if (nextAccessibleRatingUIIndex == ratingUIList.Count)
			{
				planetRatingUI = viewportContent.SpawnPlanetRating();
				ratingUIList.Add(planetRatingUI);
				planetRatingUI.transform.SetParent(transform, false);
				nextAccessibleRatingUIIndex++;
				return planetRatingUI;
			}

			planetRatingUI = ratingUIList[nextAccessibleRatingUIIndex++];
			return planetRatingUI;
		}


		public override void Hide()
		{
			//smooth lags when layout group rebuilding
			gameObject.SetActive(false);

			nextAccessibleRatingUIIndex = 0;
			int ratingUICount = ratingUIList.Count;
			for(int i = 0; i < ratingUICount; i++)
			{
				ratingUIList[i].Hide();
			}

			base.Hide();
		}
	}
}
