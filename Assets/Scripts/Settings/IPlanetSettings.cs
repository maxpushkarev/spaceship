﻿namespace spaceship
{
	internal interface IPlanetSettings : IRatingSettings
	{
		float MinScreenPercentage { get; }
		int NearestByRatingPlanetCount { get; }
	}
}
