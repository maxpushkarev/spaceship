﻿using UnityEngine;

namespace spaceship
{
	[CreateAssetMenu(fileName = "PlayerSettings", menuName = "Spaceship/Player Settings", order = SettingsUtil.PLAYER_SETTINGS_CREATE_ASSET_MENU_ORDER)]
	internal sealed class PlayerSettings : ScriptableObject, IPlayerSettings
	{
		[Header("Rating")]
		[SerializeField]
		private float minRating = 0.0f;
		[SerializeField]
		private float maxRating = 10000f;

		public float MinRating => minRating;
		public float MaxRating => maxRating;
	}
}
