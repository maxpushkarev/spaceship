﻿namespace spaceship
{
	internal interface IZoomSettings
	{
		int MinScale { get; }
		int ModeBorderScale { get; }
		int MaxScale { get; }
	}
}
