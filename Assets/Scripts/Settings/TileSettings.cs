﻿using UnityEngine;
using System;
using Random = UnityEngine.Random;
using Zenject;
using Unity.IL2CPP.CompilerServices;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
#endif

namespace spaceship
{
	[CreateAssetMenu(fileName = "TileSettings", menuName = "Spaceship/Tile Settings", order = SettingsUtil.TILE_SETTINGS_CREATE_ASSET_MENU_ORDER)]
	internal sealed class TileSettings : ScriptableObject, ITileSettings
	{
		[Header("Tile Cluster Settings")]
		[SerializeField]
		private int tileSize = 8;
		[SerializeField]
		private Tile[] tileClusterLayout;
		[SerializeField]
		private int clusterSize = 4;

#pragma warning disable 414
		[SerializeField]
		[Tooltip("Should tiles in cluster be mixed in runtime")]
		private bool mixTilesInRuntime = true;
#pragma warning restore 414

		public int TileSize => tileSize;
		public int ClusterSize => clusterSize;

		[Il2CppSetOption(Option.NullChecks, false)]
		[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
		public void GetTileAndCell(SpaceCoords spaceCoords, out Tile tile, out Cell cell)
		{
			tile = tileClusterLayout[GetLinearTileIndex(spaceCoords)];
			cell = tile.Cells[GetLinearCellIndex(spaceCoords)];
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private int GetLinearTileIndex(SpaceCoords spaceCoords) => SpaceUtils.GetLinearIndex(spaceCoords.tileCoords, clusterSize);
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private int GetLinearCellIndex(SpaceCoords spaceCoords) => SpaceUtils.GetLinearIndex(spaceCoords.cellCoords, tileSize);

		private void OnEnable()
		{
			ValidateClusterSize();
			ValidateTilesIdentity();
		}

		private void ValidateClusterSize()
		{
			if(!Mathf.IsPowerOfTwo(clusterSize))
			{
				throw new ArgumentException($"Cluster size must be power of 2 but was {clusterSize}");
			}

			if (!Mathf.IsPowerOfTwo(tileSize))
			{
				throw new ArgumentException($"Tile size must be power of 2 but was {tileSize}");
			}
		}

		private void ValidateTilesIdentity()
		{
			HashSet<Tile> tilesHashSet = new HashSet<Tile>();
			Array.ForEach(tileClusterLayout, (Tile t) => {
				if(tilesHashSet.Contains(t))
				{
					throw new InvalidDataException($"Tile {t.name} already exists in cluster");
				}
				tilesHashSet.Add(t);
			});
		}

		private void MixTilesInCluster()
		{
			int tilesCount = tileClusterLayout.Length;
			for(int i =0; i< tilesCount; i++)
			{
				int rndIndex = Random.Range(0, tilesCount);
				Tile tmp = tileClusterLayout[rndIndex];
				tileClusterLayout[rndIndex] = tileClusterLayout[i];
				tileClusterLayout[i] = tmp;
			}
		}

		[Inject]
		private void Construct(
			DiContainer container
		)
		{
			#if !UNITY_EDITOR
				MixTilesInCluster();
			#endif
			Array.ForEach(tileClusterLayout, t => container.Inject(t));
		}

#if UNITY_EDITOR

		[Header("Editor Only Settings")]
		[SerializeField]
		private string tilesGeneratedRelativeFolder = string.Empty;

		[Inject]
		private IPlanetSettings planetSettings;
		[Inject]
		private IZoomSettings zoomSettings;
		[Inject(Optional = true)]
		private DecorationSettings decorationSettings;
		[Inject]
		private DiContainer container;

		public Tile[] ClusterLayout => tileClusterLayout;
		
		internal void BuildEmptyTiles()
		{

			for (int i = 0; i < clusterSize * clusterSize; i++)
			{
				Tile spaceTile = CreateInstance<Tile>();
				spaceTile.name = $"Tile_{i + 1}";
				string assetPathAndName = $"Assets/{tilesGeneratedRelativeFolder}/{spaceTile.name}.asset";
				AssetDatabase.CreateAsset(spaceTile, assetPathAndName);
			}

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		internal void GenerateRandomTileClusterLayout()
		{
			Tile[] tilesArray = LoadAllTiles();
			int tilesArrayCount = tilesArray.Length;
			int clusterSquare = clusterSize * clusterSize;

			if (clusterSquare < tilesArrayCount)
			{
				throw new ArgumentException($"Square of cluster (current: {clusterSquare}) must be greater or equal to the tiles count (current: {tilesArrayCount})");
			}

			List<Tile> tilesList = tilesArray.ToList();
			tileClusterLayout = new Tile[clusterSquare];
			int counter = clusterSquare;

			while(counter > 0)
			{
				int tilesListCount = tilesList.Count;
				Tile randomTile = null;

				if(tilesListCount > 0)
				{
					int listIndex = Random.Range(0, tilesListCount);
					randomTile = tilesList[listIndex];
					tilesList.RemoveAt(listIndex);
				}
				else
				{
					throw new InvalidOperationException(
						$"Not enough tiles to fill the cluster. Required = {clusterSquare}, Filled = {counter}"
					);
				}

				tileClusterLayout[--counter] = randomTile;
			}

			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		internal void GenerateEmptyCells()
		{
			int sqrTileSize = tileSize * tileSize;
			Array.ForEach(tileClusterLayout, (Tile tile) =>
			{
				tile.Cells = new Cell[sqrTileSize];
				EditorUtility.SetDirty(tile);
			});
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		internal void FillCellsWithPlanets()
		{
			float minScreenPercentage = planetSettings.MinScreenPercentage;
			int modPlanetData = Mathf.FloorToInt(1.0f / minScreenPercentage);

			Array.ForEach(tileClusterLayout, (Tile tile) =>
			{
				for(int i = 0; i < tileSize; i++)
				{
					for (int j = 0; j < tileSize; j++)
					{
						int linearIndex = SpaceUtils.GetLinearIndex(new Position<int>(j, i), tileSize);
						tile.Cells[linearIndex].HasPlanet = linearIndex % modPlanetData == 0;
					}
				}
				EditorUtility.SetDirty(tile);
			});

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		internal void ValidatePlanetFillness()
		{
			for (int zoom = zoomSettings.MinScale; zoom <= zoomSettings.MaxScale; zoom++)
			{
				CheckPlanetFillnessByViewportSize(zoom);
			}
			Debug.Log("<color=green>Planet fillness validation successful!!!!!</color>");
		}

		private void CheckPlanetFillnessByViewportSize(int zoomSize)
		{
			long clusterSizeInCells = SpaceUtils.GetClusterSizeInCells(this);
			for (long i = 0; i < clusterSizeInCells; i++)
			{
				long planetCounter = CheckPlanetFillnessByViewportSizeAtPoint(i, 0, zoomSize);
				for (long j = 1; j < clusterSizeInCells; j++)
				{
					long oldRowY = j - 1;
					long oldRowCounter = CalculateRowPlanetCounter(i, oldRowY, zoomSize);

					long newRowY = j + zoomSize - 1;
					long newRowCounter = CalculateRowPlanetCounter(i, newRowY, zoomSize);

					planetCounter -= oldRowCounter;
					planetCounter += newRowCounter;

					ValidatePlanetFillness(planetCounter, zoomSize);
				}
			}
		}

		private long CalculateRowPlanetCounter(long blX, long blY, int zoomSize)
		{
			long counter = 0;

			for (int r = 0; r < zoomSize; r++)
			{
				SpaceCoords coords = new SpaceCoords(blX + r, blY, this);
				Cell cell;
				Tile tile;
				GetTileAndCell(coords, out tile, out cell);
				if (cell.Planet)
				{
					counter++;
				}
			}

			return counter;
		}

		private long CheckPlanetFillnessByViewportSizeAtPoint(long x, long y, int zoom)
		{
			long planetCounter = 0;

			for (int i = 0; i < zoom; i++)
			{
				for (int j = 0; j < zoom; j++)
				{
					SpaceCoords spaceCoords = new SpaceCoords(x + j, y + i, this);
					Cell cell;
					Tile tile;
					GetTileAndCell(spaceCoords, out tile, out cell);
					if (cell.Planet)
					{
						planetCounter++;
					}
				}
			}

			ValidatePlanetFillness(planetCounter, zoom);
			return planetCounter;
		}

		private void ValidatePlanetFillness(long planetCounter, int zoom)
		{
			float actualPercentage = ((float)planetCounter) / (zoom * zoom);
			if (actualPercentage < planetSettings.MinScreenPercentage)
			{
				throw new NotEnoughPlanetsInCellsException();
			}

		}

		internal void GenerateDecorations()
		{
			
			EditorCellDecorationBuilder[] builders = new EditorCellDecorationBuilder[] {
				container.Instantiate<TopDecorationBorderBuilder>(),
				container.Instantiate<LeftDecorationBorderBuilder>()
			};

			Array.ForEach(tileClusterLayout, (Tile tile) =>
			{
				Array.ForEach(tile.Cells, (Cell cell) =>
				{
					float rnd = Random.Range(0.0f, 1.0f);
					cell.Decorations = (rnd <= decorationSettings.CellDecorationsPossibility)
					? new Decoration[1] {
						builders[Random.Range(0, builders.Length)].BuildDecoration()
					}
					: new Decoration[0];
				});
				EditorUtility.SetDirty(tile);
			});

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		internal Tile[] LoadAllTiles()
		{
			//why AssetDatabase.LoadAllAssetsAtPath returns 0 assets;
			string tilesPath = Path.Combine(Application.dataPath, tilesGeneratedRelativeFolder);
			return Directory.GetFiles(tilesPath)
				.Select(p => AssetDatabase.LoadAssetAtPath<Tile>($"Assets/{tilesGeneratedRelativeFolder}/{Path.GetFileName(p)}"))
				.Where(o => o is Tile)
				.Select(o => o as Tile)
				.ToArray();
		}

		internal abstract class EditorCellDecorationBuilder
		{
			[Inject]
			private DecorationSettings decorationSettings;

			public Decoration BuildDecoration()
			{
				return new Decoration(
					decorationSettings.DecorationPrefabs[Random.Range(0, decorationSettings.DecorationPrefabs.Length)],
					RelativePosition,
					RelativeRollAngle
				);
			}

			public abstract Vector2 RelativePosition { get; }
			public abstract float RelativeRollAngle { get; }
		}

		internal sealed class TopDecorationBorderBuilder : EditorCellDecorationBuilder
		{
			public override Vector2 RelativePosition => new Vector2(0.0f, 0.5f);
			public override float RelativeRollAngle => 0;
		}

		internal sealed class LeftDecorationBorderBuilder : EditorCellDecorationBuilder
		{
			public override Vector2 RelativePosition => new Vector2(-0.5f, 0.0f);
			public override float RelativeRollAngle => 90;
		}
		internal class NotEnoughPlanetsInCellsException : Exception
		{
		}

#endif
	}
}
