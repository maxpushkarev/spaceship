﻿using UnityEngine;

namespace spaceship
{
	[CreateAssetMenu(fileName = "DecorationSettings", menuName = "Spaceship/Decoration Settings", order = SettingsUtil.DECORATION_SETTINGS_CREATE_ASSET_MENU_ORDER)]
	internal sealed class DecorationSettings : ScriptableObject, IDecorationSettings
	{
		[Header("General")]
		[SerializeField]
		private int maxVisibleZoom = 9;

		[Header("Prefabs")]
		[SerializeField]
		private DecorationUI[] decorationPrefabs;

#if UNITY_EDITOR
		[Header("Editor Builder")]
		[SerializeField]
		private float cellDecorationsPossibility = 0.25f;
		public float CellDecorationsPossibility => cellDecorationsPossibility;
#endif

		public int MaxVisibleZoom => maxVisibleZoom;
		public DecorationUI[] DecorationPrefabs => decorationPrefabs;
	}
}
