﻿using System;
using System.IO;
using UnityEngine;

namespace spaceship
{
	[CreateAssetMenu(fileName = "PlanetSettings", menuName = "Spaceship/Planet Settings", order = SettingsUtil.PLANET_SETTINGS_CREATE_ASSET_MENU_ORDER)]
	internal sealed class PlanetSettings : ScriptableObject, IPlanetSettings
	{
		[SerializeField]
		[Tooltip("How mush planet should be placed on the screen")]
		private float minScreenPercentage = 0.3f;

		[SerializeField]
		[Tooltip("Number of planets (nearest to player's rating) to show")]
		private int nearestByRatingPlanetCount = 20;

		[Header("Random rating")]
		[SerializeField]
		private float minRating = 0.0f;
		[SerializeField]
		private float maxRating = 10000f;

		public int NearestByRatingPlanetCount => nearestByRatingPlanetCount;
		public float MinScreenPercentage => minScreenPercentage;
		public float MinRating => minRating;
		public float MaxRating => maxRating;
	}
}
