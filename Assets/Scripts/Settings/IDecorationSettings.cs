﻿namespace spaceship
{
	internal interface IDecorationSettings
	{
		int MaxVisibleZoom { get; }
		DecorationUI[] DecorationPrefabs { get; }
	}
}
