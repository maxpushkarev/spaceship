﻿namespace spaceship
{
	internal static class SettingsUtil
	{
		public const int TILE_SETTINGS_CREATE_ASSET_MENU_ORDER = 1;
		public const int PLANET_SETTINGS_CREATE_ASSET_MENU_ORDER = 2;
		public const int ZOOM_SETTINGS_CREATE_ASSET_MENU_ORDER = 3;
		public const int PLAYER_SETTINGS_CREATE_ASSET_MENU_ORDER = 4;
		public const int DECORATION_SETTINGS_CREATE_ASSET_MENU_ORDER = 4;
	}
}
