﻿namespace spaceship
{
	internal interface IRatingSettings
	{
		float MinRating { get; }
		float MaxRating { get; }
	}
}
