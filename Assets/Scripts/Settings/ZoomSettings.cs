﻿using UnityEngine;

namespace spaceship
{
	[CreateAssetMenu(fileName = "ZoomSettings", menuName = "Spaceship/Zoom Settings", order = SettingsUtil.ZOOM_SETTINGS_CREATE_ASSET_MENU_ORDER)]
	internal sealed class ZoomSettings : ScriptableObject, IZoomSettings
	{
		[Header("Range")]
		[SerializeField]
		private int minZoomScale = 5;
		[SerializeField]
		private int maxZoomScale = 10000;
		[Header("Border")]
		[SerializeField]
		private int borderScale = 10;

		public int MinScale => minZoomScale;
		public int MaxScale => maxZoomScale;
		public int ModeBorderScale => borderScale;
	}
}
