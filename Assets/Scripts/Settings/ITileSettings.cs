﻿namespace spaceship
{
	internal interface ITileSettings
	{
		int TileSize { get; }
		int ClusterSize { get; }
		void GetTileAndCell(
			SpaceCoords spaceCoords, 
			out Tile tile, 
			out Cell cell
		);
	}
}
