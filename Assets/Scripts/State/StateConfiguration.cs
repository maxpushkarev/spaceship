﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace spaceship
{
	internal abstract class StateConfiguration<TBaseStateBehaviour> : IEnumerable<TBaseStateBehaviour>
		where TBaseStateBehaviour : BaseStateBehaviour
	{
		[SerializeField]
		private TBaseStateBehaviour[] stateBehaviours;

		private delegate void ProcessingStateBehaviour(TBaseStateBehaviour stateBehaviour);
		private ProcessingStateBehaviour onStateEnterDelegateInstance;
		private ProcessingStateBehaviour onStateExitDelegateInstance;

		public StateConfiguration()
		{
			onStateEnterDelegateInstance = (TBaseStateBehaviour stateBehaviour) => stateBehaviour.OnStateEnter();
			onStateExitDelegateInstance = (TBaseStateBehaviour stateBehaviour) => stateBehaviour.OnStateExit();
		}

		public void DisableStateBehaviours() => SetStateBehavioursEnabled(false);
		public void DisableAndSignalStateBehaviours() => SetStateBehavioursEnabled(false, onStateExitDelegateInstance);
		public void EnableAndSignalStateBehaviors() => SetStateBehavioursEnabled(true, onStateEnterDelegateInstance);

		private void SetStateBehavioursEnabled(bool enabled, ProcessingStateBehaviour processingStateBehaviour = null)
		{
			for (int i = 0; i < stateBehaviours.Length; i++)
			{
				TBaseStateBehaviour stateBehaviour = stateBehaviours[i];
				stateBehaviour.enabled = enabled;
				processingStateBehaviour?.Invoke(stateBehaviour);
			}
		}

		public void ForEach(Action<TBaseStateBehaviour> stateBehaviourAction)
		{
			for (int i = 0; i < stateBehaviours.Length; i++)
			{
				stateBehaviourAction(stateBehaviours[i]);
			}
		}

		public IEnumerator<TBaseStateBehaviour> GetEnumerator()
		{
			for(int i = 0; i < stateBehaviours.Length; i++)
			{
				yield return stateBehaviours[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
