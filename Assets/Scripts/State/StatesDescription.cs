﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace spaceship
{
	internal abstract class StatesDescription<TState, TBaseStateBehavior, TStateConfiguration> : ISerializationCallbackReceiver, IEnumerable<TStateConfiguration>
		where TState : struct
		where TBaseStateBehavior : BaseStateBehaviour
		where TStateConfiguration : StateConfiguration<TBaseStateBehavior>
	{
		[SerializeField]
		protected TStateConfiguration[] stateConfigurations;

		public StatesDescription()
		{
			stateConfigurations = new TStateConfiguration[GetStatesCount()];
		}

		public abstract TStateConfiguration this[TState type]
		{
			get;
		}

		public void ForEach(Action<TStateConfiguration> stateConfigurationAction)
		{
			for(int i = 0; i < stateConfigurations.Length; i++)
			{
				stateConfigurationAction(stateConfigurations[i]);
			}
		}

		public void OnBeforeSerialize()
		{
		}

		public void OnAfterDeserialize()
		{
			int currentCount = stateConfigurations.Length;
			int requiredCount = GetStatesCount();

			if (requiredCount == currentCount)
			{
				return;
			}

			TStateConfiguration[] newStateConfigurations = new TStateConfiguration[requiredCount];
			Array.Copy(stateConfigurations, newStateConfigurations, Math.Min(currentCount, requiredCount));
			stateConfigurations = newStateConfigurations;
		}

		internal static int GetStatesCount() => Enum.GetValues(typeof(TState)).Length;

		public IEnumerator<TStateConfiguration> GetEnumerator()
		{
			foreach (TStateConfiguration stateConfiguration in stateConfigurations)
			{
				yield return stateConfiguration;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
