﻿using UnityEngine;

namespace spaceship
{
	internal abstract class BaseStateBehaviour : MonoBehaviour
	{
		public virtual void OnStateEnter() { }
		public virtual void OnStateExit() { }
	}
}
