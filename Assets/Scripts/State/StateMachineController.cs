﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace spaceship
{
	internal abstract class StateMachineController<
		TState, 
		TBaseStateBehavior, 
		TStateConfiguration,
		TStatesDescription
	> : MonoBehaviour
		where TState : struct
		where TBaseStateBehavior : BaseStateBehaviour
		where TStateConfiguration : StateConfiguration<TBaseStateBehavior>
		where TStatesDescription : StatesDescription<TState, TBaseStateBehavior, TStateConfiguration>
	{
		[SerializeField]
		private TStatesDescription statesDescription;
		[SerializeField]
		private TState initialState;

		private TState currentState;
		private Action<TStateConfiguration> disableStateBehavioursDelegateInstance;
		protected TState CurrentState => currentState;
		protected TStatesDescription StatesDescription => statesDescription;

		private void Awake()
		{
			disableStateBehavioursDelegateInstance = DisableStateBehaviours;
			DisableAllStateBehaviours();
			SetState(initialState);
		}

		private void DisableAllStateBehaviours() => statesDescription.ForEach(disableStateBehavioursDelegateInstance);
		private void DisableStateBehaviours(TStateConfiguration stateConfiguration) => stateConfiguration.DisableStateBehaviours();

		private void SetState(TState state)
		{
			statesDescription[state].EnableAndSignalStateBehaviors();
			currentState = state;
		}

		public void SwitchState(TState newState)
		{
			if(EqualityComparer<TState>.Default.Equals(currentState, newState))
			{
				return;
			}

			statesDescription[currentState].DisableAndSignalStateBehaviours();
			SetState(newState);
		}
	}
}
