﻿using System;

namespace spaceship
{
	[Serializable]
	internal sealed class ViewportUIStatesDescription : StatesDescription<ZoomStates, ViewportUIStateBehaviour, ViewportUIStateConfiguration>
	{
		public override ViewportUIStateConfiguration this[ZoomStates state] => stateConfigurations[(int)state];
	}
}
