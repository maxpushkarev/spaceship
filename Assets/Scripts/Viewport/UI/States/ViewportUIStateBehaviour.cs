﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace spaceship
{
	[RequireComponent(typeof(Image))]
	internal abstract class ViewportUIStateBehaviour : BaseStateBehaviour
	{
		[SerializeField]
		private ViewportUIGridState gridState;

		private Image gridImg;
		private Image GridImg
		{
			get
			{
				gridImg = gridImg ?? GetComponent<Image>();
				return gridImg;
			}
		}

		public override void OnStateEnter()
		{
			GridImg.color = gridState.color;
			GridImg.sprite = gridState.sprite;
		}

#pragma warning disable 649

		[Serializable]
		private class ViewportUIGridState
		{
			public Sprite sprite;
			public Color color;
		}

#pragma warning restore 649
	}
}
