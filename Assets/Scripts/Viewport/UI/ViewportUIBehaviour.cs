﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace spaceship
{
	[RequireComponent(typeof(Image))]
	internal sealed class ViewportUIBehaviour : MonoBehaviour
	{
		private IZoomSettings zoomSettings;

		[SerializeField]
		private Image gridImage;
		private Material gridImageMat;

		private void Awake()
		{
			gridImageMat = Instantiate(gridImage.material);
			gridImage.material = gridImageMat;
		}

		[Inject]
		private void Construct(IZoomSettings zoomSettings) => this.zoomSettings = zoomSettings;

		public void UpdateVisualGrid(int newZoom)
		{
			float tile = ((float)newZoom) / zoomSettings.MinScale;
			gridImageMat.mainTextureScale = new Vector2(tile, tile);
		}
	}
}
