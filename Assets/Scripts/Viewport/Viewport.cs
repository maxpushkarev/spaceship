﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Zenject;
using Random = UnityEngine.Random;

namespace spaceship
{
	internal sealed class Viewport : MonoBehaviour, IViewport, IInitializable
	{
		[SerializeField]
		private OnViewportUpdatedEvent onViewportUpdatedEvent;
		[Header("Rnd viewport center")]
		[SerializeField]
		private int minCenterValue = -200000;
		[SerializeField]
		private int maxCenterValue = 200000;

		private ITileSettings tileSettings;
		private SpaceCoords center;
		private int size;

		public SpaceCoords Center => center;
		public int Size => size;

		[Inject]
		private void Construct(ITileSettings tileSettings) => this.tileSettings = tileSettings;

		[Inject]
		public void Initialize()
		{
			center = new SpaceCoords(
				Random.Range(minCenterValue, maxCenterValue),
				Random.Range(minCenterValue, maxCenterValue),
				tileSettings
			);
		}

		public void OnZoomUpdated(int newZoom)
		{
			this.size = newZoom;
			onViewportUpdatedEvent.Invoke();
		}

		public void OnInputMoved(SpaceCoords movement)
		{
			this.center += movement;
			onViewportUpdatedEvent.Invoke();
		}

		[Serializable]
		private class OnViewportUpdatedEvent : UnityEvent
		{
		}
	}
}
