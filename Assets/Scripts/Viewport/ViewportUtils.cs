﻿namespace spaceship
{
	internal static class ViewportUtils
	{
		public static SpaceCoords GetBottomLeft(IViewport viewport, ITileSettings tileSettings)
		{
			Position<long> center = viewport.Center.worldCoords;
			int halfSize = viewport.Size / 2;
			return new SpaceCoords(
				center.x - halfSize, 
				center.y - halfSize, 
				tileSettings
			);
		}
	}
}
