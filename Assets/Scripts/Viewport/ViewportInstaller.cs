﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class ViewportInstaller : MonoInstaller
	{
		[SerializeField]
		private Viewport viewport;

		public override void InstallBindings()
		{
			Container.BindInstance<IViewport>(viewport);
		}
	}
}