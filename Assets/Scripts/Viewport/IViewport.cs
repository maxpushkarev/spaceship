﻿namespace spaceship
{
	internal interface IViewport
	{
		SpaceCoords Center { get; }
		int Size { get; }
	}
}
