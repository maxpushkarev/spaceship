﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class ViewportContent : MonoBehaviour
	{
		[Header("General")]
		[SerializeField]
		private int hierarchyCapacity = 64;
		[SerializeField]
		private RectTransform rootViewportRectTransform;
		[SerializeField]
		private ViewportContentItem spaceshipItem;

		[Header("Planets")]
		[SerializeField]
		private RectTransform planetsRootRectTransform;
		[SerializeField]
		private RectTransform planetsRatingUIRootRectTransform;
		[SerializeField]
		private PlanetUI planetUiPrefab;
		[SerializeField]
		private PlanetRatingUI planetRatingUI;

		[Header("Decorations")]
		private int initialDecorationCapacity = 50;
		[SerializeField]
		private RectTransform decorationsRootRectTransform;

		private DecorationUIFactoryResolver decorationUIFactoryResolver;
		private PlanetRatingUIFactory planetRatingUIFactory;
		private PlanetUIFactory planetUIFactory;
		private List<PlanetUI> activePlanets;
		private List<DecorationUI> activeDecorations;

		[Inject]
		private void Construct(
			PlanetUIFactory planetUIFactory,
			PlanetRatingUIFactory planetRatingUIFactory,
			IPlanetSettings planetSettings,
			IZoomSettings zoomSettings,
			IDecorationSettings decorationSettings,
			DecorationUIFactoryResolver decorationUIFactoryResolver
		)
		{
			this.decorationUIFactoryResolver = decorationUIFactoryResolver;
			this.planetRatingUIFactory = planetRatingUIFactory;
			this.planetUIFactory = planetUIFactory;
			rootViewportRectTransform.hierarchyCapacity = hierarchyCapacity;
			int lastPlanetNormalModeScale = zoomSettings.ModeBorderScale - 1;
			this.activePlanets = new List<PlanetUI>(
				Mathf.Max(
					planetSettings.NearestByRatingPlanetCount, 
					lastPlanetNormalModeScale * lastPlanetNormalModeScale
					)
			);
			this.activeDecorations = new List<DecorationUI>(
				initialDecorationCapacity
			);
		}

		public ViewportContentItem SpaceshipItem => spaceshipItem;
		public Vector2 ViewportUISize => rootViewportRectTransform.sizeDelta;
		public PlanetUI PlanetUIPrefab => planetUiPrefab;
		public RectTransform PlanetsRootRectTransform => planetsRootRectTransform;
		public RectTransform PlanetsRatingUIRootRectTransform => planetsRatingUIRootRectTransform;
		public PlanetRatingUI PlanetRatingUI => planetRatingUI;
		public RectTransform DecorationsRootRectTransform => decorationsRootRectTransform;

		private void HideAllPoolableElements<T>(List<T> activeElements) where T : BaseViewportContentPoolableUI
		{
			int count = activeElements.Count;
			for (int i = 0; i < count; i++)
			{
				activeElements[i].Hide();
			}
			activeElements.Clear();
		}

		#region DECORATIONS

		public void EnableDecorations() => decorationsRootRectTransform.gameObject.SetActive(true);
		public void DisableDecorations() => decorationsRootRectTransform.gameObject.SetActive(false);
		public void HideAllDecorations() => HideAllPoolableElements(activeDecorations);
		public DecorationUI SpawnDecoration(DecorationUI decorationUIPrefab)
		{
			DecorationUI instance = decorationUIFactoryResolver.SpawnDecorationUI(decorationUIPrefab);
			activeDecorations.Add(instance);
			return instance;
		}

		#endregion

		#region PLANETS

		public void HideAllPlanets() => HideAllPoolableElements(activePlanets);
		
		public PlanetUI SpawnPlanet()
		{
			PlanetUI spawnedPlanetUI = planetUIFactory.Create();
			activePlanets.Add(spawnedPlanetUI);
			return spawnedPlanetUI;
		}

		public PlanetRatingUI SpawnPlanetRating()
		{
			PlanetRatingUI spawnedPlanetRatingUI = planetRatingUIFactory.Create();
			return spawnedPlanetRatingUI;
		}

		#endregion
	}
}
