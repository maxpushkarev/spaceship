﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace spaceship
{
	[RequireComponent(typeof(ViewportContent))]
	internal sealed class ViewportContentInstaller : MonoInstaller
	{
		[SerializeField]
		private ViewportContent viewportContent;
		[SerializeField]
		private int initialPlanetPoolSize = 15;
		[SerializeField]
		private int initialDecorationPoolSize = 3;

		[Inject]
		private IDecorationSettings decorationSettings;

		public override void InstallBindings()
		{
			BindFactoryWithPool<PlanetUI, PlanetUIFactory, PlanetUIPool>(
				viewportContent.PlanetUIPrefab,
				viewportContent.PlanetsRootRectTransform.transform,
				initialPlanetPoolSize,
				Container
			);

			BindFactoryWithPool<PlanetRatingUI, PlanetRatingUIFactory, PlanetRatingUIPool>(
				viewportContent.PlanetRatingUI,
				viewportContent.PlanetsRatingUIRootRectTransform.transform,
				initialPlanetPoolSize,
				Container
			);

			Dictionary<DecorationUI, DecorationUIBuilder> prefab2BuilderMap = new Dictionary<DecorationUI, DecorationUIBuilder>(
				decorationSettings.DecorationPrefabs.Length
			);

			Array.ForEach(decorationSettings.DecorationPrefabs, dUI =>
			{
				DiContainer childContainer = Container.CreateSubContainer();
				BindFactoryWithPool<DecorationUI, DecorationUIFactory, DecorationUIPool>(
					dUI,
					viewportContent.DecorationsRootRectTransform,
					initialDecorationPoolSize,
					childContainer
				);
				DecorationUIBuilder decorationUIBuilder = childContainer.Instantiate<DecorationUIBuilder>();
				prefab2BuilderMap.Add(dUI, decorationUIBuilder);
			});

			Container.BindInstance<DecorationUIFactoryResolver>(
				new DecorationUIFactoryResolver(prefab2BuilderMap)
			);
			Container.BindInstance<ViewportContent>(viewportContent);
		}

		private void BindFactoryWithPool<TComponent, TFactory, TPool>(
			TComponent prefabComponent, 
			Transform poolingTransform, 
			int initialPoolSize,
			DiContainer container
		)
			where TComponent : BaseViewportContentPoolableUI
			where TFactory : PlaceholderFactory<TComponent>
			where TPool : MonoPoolableMemoryPool<IMemoryPool, TComponent>
		{
			container.BindFactory<TComponent, TFactory>()
				.FromPoolableMemoryPool<TComponent, TPool>(poolBinder => poolBinder
					.WithInitialSize(initialPoolSize)
					.FromComponentInNewPrefab(prefabComponent)
					.UnderTransform(poolingTransform)
				);
		}

		private class PlanetUIPool : MonoPoolableMemoryPool<IMemoryPool, PlanetUI>
		{
		}

		private class DecorationUIPool : MonoPoolableMemoryPool<IMemoryPool, DecorationUI>
		{
		}

		private class PlanetRatingUIPool : MonoPoolableMemoryPool<IMemoryPool, PlanetRatingUI>
		{
		}
	}
}
