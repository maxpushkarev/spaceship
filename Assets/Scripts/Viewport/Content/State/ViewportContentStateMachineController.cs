﻿using System;
using System.Collections.Generic;

namespace spaceship
{
	internal sealed class ViewportContentStateMachineController
		: StateMachineController<ZoomStates, ViewportContentStateBehaviour, ViewportContentStateConfiguration, ViewportContentStatesDescription>
	{
		private Action<ViewportContentStateBehaviour> updateShipDelegateInstance;
		private Action<ViewportContentStateBehaviour> updatePlanetsDelegateInstance;
		private List<PlanetEntry> cachedListEntry;

		private void Awake()
		{
			updateShipDelegateInstance = UpdateShip;
			updatePlanetsDelegateInstance = OnPlanetsSelected;
		}

		public void UpdateShipWhenViewportUpdated() => StatesDescription[CurrentState].ForEach(updateShipDelegateInstance);
		private void UpdateShip(ViewportContentStateBehaviour viewportContentStateBehaviour) =>
			viewportContentStateBehaviour.UpdateShip();

		public void OnPlanetsSelected(List<PlanetEntry> selectedPlanets)
		{
			cachedListEntry = selectedPlanets;
			StatesDescription[CurrentState].ForEach(updatePlanetsDelegateInstance);
			cachedListEntry = null;
		}

		public void OnPlanetsSelected(ViewportContentStateBehaviour viewportContentStateBehaviour)
		{
			viewportContentStateBehaviour.UpdatePlanets(cachedListEntry);
		}
	}
}
