﻿using System.Collections.Generic;
using UnityEngine;

namespace spaceship
{
	internal sealed class ViewportContentNormalStateBehaviour : ViewportContentStateBehaviour
	{
		public override void UpdateShip()
		{
			Position<long> worldShipCellPos = Viewport.Center.worldCoords;
			SpaceCoords blVp = ViewportUtils.GetBottomLeft(Viewport, TileSettings);
			Position<long> worldBlVp = blVp.worldCoords;
			Vector2 cellUISize = ViewportContentUtils.CalculateCellUISize(Viewport, ViewportContent);
			Vector2 shipUIAnchoredPos = ViewportContentUtils.CalculateCellAnchoredPosition(worldShipCellPos, worldBlVp, cellUISize);
			RectTransform spaceshipRectTransform = ViewportContent.SpaceshipItem.RectTransform;
			spaceshipRectTransform.anchoredPosition = shipUIAnchoredPos;
			spaceshipRectTransform.localScale = CalculateScale();
		}

		public override void UpdatePlanets(List<PlanetEntry> selectedPlanets)
		{
			ViewportContent.HideAllPlanets();

			SpaceCoords blVp = ViewportUtils.GetBottomLeft(Viewport, TileSettings);
			Position<long> worldBlVp = blVp.worldCoords;
			Vector3 scale = CalculateScale();
			Vector2 cellUISize = ViewportContentUtils.CalculateCellUISize(Viewport, ViewportContent);

			int selectedCount = selectedPlanets.Count;
			for(int i = 0; i < selectedCount; i++)
			{
				PlanetEntry planetEntry = selectedPlanets[i];
				PlanetUI planetUI = ViewportContent.SpawnPlanet();
				PlanetRatingUI planetRatingUI = planetUI.CreatePlanetRatingUI();
				planetRatingUI.Prepare(GetStrRating(planetEntry));

				Position<long> worldPlanetPos = planetEntry.worldPos;
				Vector2 anchoredPlanetPos = ViewportContentUtils.CalculateCellAnchoredPosition(worldPlanetPos, worldBlVp, cellUISize);
				RectTransform planetRectTransform = planetUI.Item.RectTransform;
				planetRectTransform.anchoredPosition = anchoredPlanetPos;
				planetRectTransform.localScale = scale;
			}
		}

		protected override Vector3 CalculateScale() => ViewportContentUtils.CalculateScale(ZoomSettings, Viewport.Size);
	}
}
