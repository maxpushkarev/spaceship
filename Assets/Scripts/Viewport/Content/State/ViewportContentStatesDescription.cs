﻿using System;

namespace spaceship
{
	[Serializable]
	internal sealed class ViewportContentStatesDescription : StatesDescription<ZoomStates, ViewportContentStateBehaviour, ViewportContentStateConfiguration>
	{
		public override ViewportContentStateConfiguration this[ZoomStates state] => stateConfigurations[(int)state];
	}
}
