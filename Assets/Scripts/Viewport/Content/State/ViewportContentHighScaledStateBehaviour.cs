﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class ViewportContentHighScaledStateBehaviour : ViewportContentStateBehaviour
	{
		private Dictionary<AlignedPlanetMarker, PlanetUI> alignedPlanetsMap;

		[Inject]
		private void Construct(IPlanetSettings planetSettings)
		{
			alignedPlanetsMap = new Dictionary<AlignedPlanetMarker, PlanetUI>(planetSettings.NearestByRatingPlanetCount);
		}

		public override void UpdateShip()
		{
			SpaceCoords blVp = ViewportUtils.GetBottomLeft(Viewport, TileSettings);
			Position<long> worldBlVp = blVp.worldCoords;
			Position<long> worldShipCellPos = Viewport.Center.worldCoords;
			Vector2 cellUISize = ViewportContentUtils.CalculateCellUISize(Viewport, ViewportContent);
			Vector2 shipUIAnchoredPos = ViewportContentUtils.CalculateCellAnchoredPosition(worldShipCellPos, worldBlVp, cellUISize);
			RectTransform spaceshipRectTransform = ViewportContent.SpaceshipItem.RectTransform;
			Vector2Int alignedChunkCoords;
			Vector2 alignedShipAnchoredPos = AlignAnchoredPos(shipUIAnchoredPos, out alignedChunkCoords);
			spaceshipRectTransform.anchoredPosition = alignedShipAnchoredPos;
			spaceshipRectTransform.localScale = CalculateScale();
		}

		public override void UpdatePlanets(List<PlanetEntry> selectedPlanets)
		{
			ViewportContent.HideAllPlanets();
			alignedPlanetsMap.Clear();
			Vector3 scale = CalculateScale();

			SpaceCoords blVp = ViewportUtils.GetBottomLeft(Viewport, TileSettings);
			Position<long> worldBlVp = blVp.worldCoords;
			Vector2 cellUISize = ViewportContentUtils.CalculateCellUISize(Viewport, ViewportContent);

			int selectedCount = selectedPlanets.Count;
			for (int i = 0; i < selectedCount; i++)
			{
				PlanetEntry planetEntry = selectedPlanets[i];
				Vector2Int alignedChunkCoords;
				Position<long> worldPlanetCellPos = planetEntry.worldPos;
				Vector2 planetUIAnchoredPos = ViewportContentUtils.CalculateCellAnchoredPosition(worldPlanetCellPos, worldBlVp, cellUISize);
				Vector2 alignedPlanetAnchoredPos = AlignAnchoredPos(planetUIAnchoredPos, out alignedChunkCoords);
				AlignedPlanetMarker alignedPlanetMarker = new AlignedPlanetMarker(alignedChunkCoords);

				PlanetUI spawnedPlanet;
				
				if (!alignedPlanetsMap.TryGetValue(alignedPlanetMarker, out spawnedPlanet))
				{
					spawnedPlanet = ViewportContent.SpawnPlanet();
					alignedPlanetsMap.Add(alignedPlanetMarker, spawnedPlanet);

					RectTransform planetRectTransform = spawnedPlanet.Item.RectTransform;
					planetRectTransform.anchoredPosition = alignedPlanetAnchoredPos;
					planetRectTransform.localScale = scale;
				}

				PlanetRatingUI planetRatingUI = spawnedPlanet.CreatePlanetRatingUI();
				planetRatingUI.Prepare(GetStrRating(planetEntry));
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private Vector2 AlignAnchoredPos(Vector2 inputItemAnchoredPos, out Vector2Int alignedChunksCoords)
		{
			Vector2 uiChunks = ViewportContent.ViewportUISize / GetTargetScale();
			alignedChunksCoords = new Vector2Int(
				Mathf.FloorToInt(inputItemAnchoredPos.x / uiChunks.x),
				Mathf.FloorToInt(inputItemAnchoredPos.y / uiChunks.y)
			);
			return new Vector2(
				alignedChunksCoords.x * uiChunks.x + 0.5f * uiChunks.x,
				alignedChunksCoords.y * uiChunks.y + 0.5f * uiChunks.y
			);
		}

		private int GetTargetScale() => (ZoomSettings.ModeBorderScale % 2 == 0) 
			? ZoomSettings.ModeBorderScale - 1 
			: ZoomSettings.ModeBorderScale;

		protected override Vector3 CalculateScale() => ViewportContentUtils.CalculateScale(ZoomSettings, GetTargetScale());

		private struct AlignedPlanetMarker : IEquatable<AlignedPlanetMarker>
		{
			public readonly Vector2Int alignedUIChunkCoords;
			public AlignedPlanetMarker(Vector2Int alignedUIChunkCoords)
			{
				this.alignedUIChunkCoords = alignedUIChunkCoords;
			}

			public override int GetHashCode() => alignedUIChunkCoords.GetHashCode();
			public override bool Equals(object obj) => Equals( (AlignedPlanetMarker) obj );
			public bool Equals(AlignedPlanetMarker other) =>
				alignedUIChunkCoords.x.Equals(other.alignedUIChunkCoords.x) &&
				alignedUIChunkCoords.y.Equals(other.alignedUIChunkCoords.y);
		}

	}
}
