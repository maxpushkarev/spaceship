﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal abstract class ViewportContentStateBehaviour : BaseStateBehaviour
	{
		protected ViewportContent ViewportContent { get; private set; }
		protected IViewport Viewport { get; private set; }
		protected ITileSettings TileSettings { get; private set; }
		protected IZoomSettings ZoomSettings { get; private set; }

		[Inject]
		private void Construct(
			IViewport viewport,
			ITileSettings tileSettings,
			ViewportContent viewportContent,
			IZoomSettings zoomSettings
		)
		{
			this.Viewport = viewport;
			this.TileSettings = tileSettings;
			this.ZoomSettings = zoomSettings;
			this.ViewportContent = viewportContent;
		}

		protected static string GetStrRating(PlanetEntry entry) => Mathf.RoundToInt(entry.planet.rating).ToString();

		public abstract void UpdateShip();
		public abstract void UpdatePlanets(List<PlanetEntry> selectedPlanets);
		protected abstract Vector3 CalculateScale();
	}
}
