﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal abstract class BaseViewportContentPoolableUI : MonoBehaviour, IPoolable<IMemoryPool>
	{
		private IMemoryPool memoryPool;

		public void OnDespawned() { }
		public void OnSpawned(IMemoryPool memoryPool) => this.memoryPool = memoryPool;
		public virtual void Hide() => this.memoryPool.Despawn(this);
	}
}
