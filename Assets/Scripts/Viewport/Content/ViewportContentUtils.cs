﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace spaceship
{
	internal static class ViewportContentUtils
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Vector3 CalculateScale(IZoomSettings zoomSettings, int size) => Vector3.one * (((float)(zoomSettings.MinScale)) / size);
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Vector2 CalculateCellUISize(IViewport viewport, ViewportContent viewportContent)
		{
			Vector2 viewportUISize = viewportContent.ViewportUISize;
			int logicalViewportSize = viewport.Size;
			return viewportUISize / logicalViewportSize;
		}
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Vector2 CalculateCellAnchoredPosition(
			Position<long> worldItemPos,
			Position<long> worldBlVp,
			Vector2 cellUISize
		)
		{
			Position<long> delta = new Position<long>(
				worldItemPos.x - worldBlVp.x,
				worldItemPos.y - worldBlVp.y
			);
			Vector2 res = new Vector2(
				cellUISize.x * delta.x,
				cellUISize.y * delta.y
			);
			res += 0.5f * cellUISize;
			return res;
		}
	}
}
