﻿using UnityEngine;

namespace spaceship
{
	internal sealed class ViewportContentItem : MonoBehaviour
	{
		[SerializeField]
		private RectTransform rectTransform;
		public RectTransform RectTransform => rectTransform;
	}
}
