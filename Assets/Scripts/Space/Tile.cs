﻿using System;
using UnityEngine;
using Zenject;

namespace spaceship
{
	internal sealed class Tile : ScriptableObject
	{
		[SerializeField]
		[HideInInspector]
		private Cell[] cells;
		public Cell[] Cells
		{
			get { return cells; }
			#if UNITY_EDITOR
			set { cells = value; }
			#endif
		}

		[Inject]
		private void Construct(
			ITileSettings tileSettings,
			IPlanetSettings planetSettings,
			IPlayer player
		)
		{
			SpaceCoords blTileCoords = GetBottomLeftTileCoords(tileSettings);
			int tileSize = tileSettings.TileSize;

			for (int i = 0; i < tileSize; i++)
			{
				for (int j = 0; j < tileSize; j++)
				{
					int linearIndex = SpaceUtils.GetLinearIndex(new Position<int>(j,i), tileSize);
					SpaceCoords planetCoords = blTileCoords;
					planetCoords += new SpaceCoords(j, i, tileSettings);
					//it's too slow to inject every cell through zenject
					cells[linearIndex].Init(
						planetSettings,
						player,
						planetCoords
					);
				}
			}
		}

		private SpaceCoords GetBottomLeftTileCoords(ITileSettings tileSettings)
		{
			int tileSize = tileSettings.TileSize;
			long clusterSizeInCells = SpaceUtils.GetClusterSizeInCells(tileSettings);
			for(long i = 0; i < clusterSizeInCells; i += tileSize)
			{
				for (long j = 0; j < clusterSizeInCells; j += tileSize)
				{
					SpaceCoords coords = new SpaceCoords(i, j, tileSettings);
					Tile tile;
					Cell cell;
					tileSettings.GetTileAndCell(coords, out tile, out cell);
					if(ReferenceEquals(tile, this))
					{
						return coords;
					}
				}
			}

			throw new Exception("Impossible");
		}
	}
}
