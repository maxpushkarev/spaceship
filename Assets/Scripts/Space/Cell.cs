﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace spaceship
{
	[Serializable]
	internal sealed class Cell
	{
		[SerializeField]
		private bool hasPlanet;
		[SerializeField]
		private Decoration[] decorations;

		public Decoration[] Decorations {
			get { return decorations; }
#if UNITY_EDITOR
			set { decorations = value; }
#endif
		}
		
		public Planet Planet { get; private set; }
#if UNITY_EDITOR
		public bool HasPlanet { set { hasPlanet = value; } }
#endif
		public void Init(
			IPlanetSettings planetSettings, 
			IPlayer player,
			SpaceCoords planetCoords
		)
		{
				Planet = hasPlanet ? new Planet(
					Mathf.RoundToInt(Random.Range(planetSettings.MinRating, planetSettings.MaxRating)),
					planetCoords,
					player
				) : null;
		}
	}
}
