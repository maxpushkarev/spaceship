﻿using System.Runtime.CompilerServices;

namespace spaceship
{
	internal static class SpaceUtils
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int GetLinearIndex(Position<int> indices, int size) => indices.x + indices.y * size;
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long GetClusterSizeInCells(ITileSettings tileSettings) => tileSettings.ClusterSize * tileSettings.TileSize;
	}
}
