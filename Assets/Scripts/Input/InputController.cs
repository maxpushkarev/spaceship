﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	internal abstract class InputController : MonoBehaviour
	{
		protected IInputBindings inputBindings;

		[Inject]
		private void Construct(IInputBindings inputBindings) => this.inputBindings = inputBindings;
	}
}
