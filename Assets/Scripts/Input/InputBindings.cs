﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using Zenject;

namespace spaceship
{
	[CreateAssetMenu(fileName = "InputBinding", menuName = "Spaceship/Input", order = 1)]
	internal sealed class InputBindings : ScriptableObject, IInputBindings
	{
		[Header("Movement")]
		[SerializeField]
		private string horizontalAxisName = "Horizontal";
		[SerializeField]
		private string verticalAxisName = "Vertical";

		[Header("Zoom")]
		[SerializeField]
		private float zoomSpeed = 1.0f;
		[SerializeField]
		private string zoomScrollAxisName = "ZoomScroll";
		[SerializeField]
		private string zoomKeyboardAxisName = "ZoomKeyboard";
		[SerializeField]
		private string zoomBoostAxisName = "ZoomBoostAxis";
		[SerializeField]
		private float zoomBoostValue = 5.0f;

		private ITileSettings tileSettings;
		private float rawZoomInput;

		[Inject]
		private void Construct(ITileSettings tileSettings) => this.tileSettings = tileSettings;

		public SpaceCoords? ResolveMovementInput()
		{
			SpaceCoords movementDirCoords = new SpaceCoords(
				GetMovementButtonDown(horizontalAxisName),
				GetMovementButtonDown(verticalAxisName),
				tileSettings
			);
			
			if(movementDirCoords.Equals(SpaceCoords.ZERO))
			{
				return null;
			}

			return movementDirCoords;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private int GetMovementButtonDown(string axisName) => Input.GetButtonDown(axisName) ? Mathf.CeilToInt(Input.GetAxis(axisName)) : 0;

		public int? ResolveZoomInput()
		{
			float scrollAxis = Input.GetAxis(zoomScrollAxisName);
			float keyboardAxis = Input.GetAxis(zoomKeyboardAxisName) * Convert.ToSingle(Input.GetButton(zoomKeyboardAxisName));
			
			float axisVal = Mathf.Lerp(scrollAxis, keyboardAxis, Convert.ToSingle(Mathf.Abs(keyboardAxis) > Mathf.Abs(scrollAxis)));
			float delta = axisVal * zoomSpeed * Time.deltaTime;
			delta = Mathf.Lerp(delta, delta * zoomBoostValue, Convert.ToSingle(Input.GetButton(zoomBoostAxisName)));
			rawZoomInput += delta;

			int val = Convert.ToInt32(Mathf.Sign(rawZoomInput) * Mathf.FloorToInt(Mathf.Abs(rawZoomInput)));
			if (val == 0)
			{
				return null;
			}

			rawZoomInput = 0.0f;
			return val;
		}
	}
}
