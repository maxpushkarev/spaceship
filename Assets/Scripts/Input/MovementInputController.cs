﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace spaceship
{
	internal sealed class MovementInputController : InputController
	{
		[SerializeField]
		private SpaceMovementEvent onMovementInputEvent;

		private void Update()
		{
			SpaceCoords? movementDirection = inputBindings.ResolveMovementInput();
			if(movementDirection.HasValue)
			{
				onMovementInputEvent.Invoke(movementDirection.Value);
			}
		}

		[Serializable]
		private class SpaceMovementEvent : UnityEvent<SpaceCoords>
		{
		}

	}
}