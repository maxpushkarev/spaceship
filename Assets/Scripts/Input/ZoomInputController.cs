﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace spaceship
{
	internal sealed class ZoomInputController : InputController
	{
		[SerializeField]
		private ZoomInputEvent onZoomInputEvent;

		private void Update()
		{
			int? zoomInput = inputBindings.ResolveZoomInput();
			if(zoomInput.HasValue)
			{
				onZoomInputEvent.Invoke(zoomInput.Value);
			}
		}

		[Serializable]
		private class ZoomInputEvent : UnityEvent<int>
		{
		}
	}
}
