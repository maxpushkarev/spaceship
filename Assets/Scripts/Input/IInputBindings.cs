﻿namespace spaceship
{
	internal interface IInputBindings
	{
		SpaceCoords? ResolveMovementInput();
		int? ResolveZoomInput();
	}
}
