﻿using UnityEngine;
using Zenject;

namespace spaceship
{
	[CreateAssetMenu(fileName = "RuntimeConfiguration", menuName = "Spaceship/Config/Runtime", order = 4)]
	internal sealed class RuntimeConfiguration : ScriptableObjectInstaller<RuntimeConfiguration>
	{
		[Header("Runtime settings")]
		[SerializeField]
		private TileSettings tileSettings;
		[SerializeField]
		private PlanetSettings planetSettings;
		[SerializeField]
		private ZoomSettings zoomSettings;
		[SerializeField]
		private PlayerSettings playerSettings;
		[SerializeField]
		private InputBindings inputBindings;
		[SerializeField]
		private DecorationSettings decorationSettings;

		public override void InstallBindings()
		{
			Container.BindInstance<IInputBindings>(inputBindings);
			Container.BindInstance<IPlanetSettings>(planetSettings);
			Container.BindInstance<IZoomSettings>(zoomSettings);
			Container.BindInstance<IPlayerSettings>(playerSettings);
			Container.BindInstance<IDecorationSettings>(decorationSettings);
			Container.BindInstance<ITileSettings>(tileSettings);
		}
	}
}
