﻿using NUnit.Framework;

namespace spaceship.test
{
	internal sealed class PositionTests
	{
		[Test]
		public void ShouldPositionsBeEquals()
		{
			Assert.AreEqual(new Position<int>(1, 2), new Position<int>(1, 2));
		}

		[Test]
		public void ShouldPositionsNotBeEquals()
		{
			Assert.AreNotEqual(new Position<int>(1, 2), new Position<int>(2, 1));
		}
	}
}