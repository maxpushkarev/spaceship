﻿using NUnit.Framework;
using System;

namespace spaceship.test
{
	internal sealed class SpaceCoordsTests
	{
		private ITileSettings testTileSettings;

		[SetUp]
		public void Setup()
		{
			this.testTileSettings = new TestTileSettings();
		}

		[Test]
		public void ShouldSpaceCoordsBeEquals()
		{
			Assert.AreEqual(new SpaceCoords(1, 2, testTileSettings), new SpaceCoords(1, 2, testTileSettings));
		}

		[Test]
		public void ShouldSpaceCoordsNotBeEquals()
		{
			Assert.AreNotEqual(new SpaceCoords(1, 2, testTileSettings), new SpaceCoords(1, 3, testTileSettings));
		}

		[Test]
		public void ShouldSpaceCoordsBeAdditive()
		{
			Assert.AreEqual(new SpaceCoords(7, 7, testTileSettings), new SpaceCoords(5, 9, testTileSettings) + new SpaceCoords(2, -2, testTileSettings));
		}

		[Test]
		public void ShouldSpaceCoordsBeSubtractive()
		{
			Assert.AreEqual(new SpaceCoords(-3, 11, testTileSettings), new SpaceCoords(5, 9, testTileSettings) - new SpaceCoords(8, -2, testTileSettings));
		}

		[Test]
		public void ShouldCalculateDistantClusterCoordsCorrectly()
		{
			Assert.AreEqual(new Position<long>(225, -190), new SpaceCoords(4503, -3789, testTileSettings).clusterCoords);
		}

		[Test]
		public void ShouldCalculateNearZeroClusterCoordsCorrectly()
		{
			Assert.AreEqual(new Position<long>(1, -1), new SpaceCoords(20, -20, testTileSettings).clusterCoords);
			Assert.AreEqual(new Position<long>(1, -2), new SpaceCoords(21, -21, testTileSettings).clusterCoords);
		}

		[Test]
		public void ShouldCalculateTileCoordsCorrectly()
		{
			Assert.AreEqual(new Position<int>(0, 3), new SpaceCoords(22, -1, testTileSettings).tileCoords);
			Assert.AreEqual(new Position<int>(1, 2), new SpaceCoords(27, -6, testTileSettings).tileCoords);
			Assert.AreEqual(new Position<int>(1, 3), new SpaceCoords(27, -41, testTileSettings).tileCoords);
			Assert.AreEqual(new Position<int>(1, 2), new SpaceCoords(45, -47, testTileSettings).tileCoords);
		}

		[Test]
		public void ShouldCalculateCellCoordsCorrectly()
		{
			Assert.AreEqual(new Position<int>(2, 4), new SpaceCoords(22, -1, testTileSettings).cellCoords);
			Assert.AreEqual(new Position<int>(2, 4), new SpaceCoords(27, -6, testTileSettings).cellCoords);
			Assert.AreEqual(new Position<int>(0, 3), new SpaceCoords(30, -42, testTileSettings).cellCoords);
			Assert.AreEqual(new Position<int>(1, 3), new SpaceCoords(46, -47, testTileSettings).cellCoords);
		}

		private class TestTileSettings : ITileSettings
		{
			public int TileSize => 5;
			public int ClusterSize => 4;
			public void GetTileAndCell(
				SpaceCoords spaceCoords,
				out Tile tile,
				out Cell cell
			)
			{
				throw new NotImplementedException();
			}
		}

	}
}
