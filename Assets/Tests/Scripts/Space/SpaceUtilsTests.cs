﻿using NUnit.Framework;
using System;

namespace spaceship.test
{
	internal sealed class SpaceUtilsTests
	{
		[Test]
		public void ShouldCorrectlyCalculateClusterSizeInCells()
		{
			Assert.AreEqual(256 * 32, SpaceUtils.GetClusterSizeInCells(new TestTileSettings()));
		}

		[Test]
		public void ShouldCorrectlyCalculateLinearIndex()
		{
			Assert.AreEqual(44, SpaceUtils.GetLinearIndex(new Position<int>(4, 5), 8));
		}

		private class TestTileSettings : ITileSettings
		{
			public int TileSize => 256;
			public int ClusterSize => 32;
			public void GetTileAndCell(SpaceCoords spaceCoords, out Tile tile, out Cell cell)
			{
				throw new NotImplementedException();
			}
		}
	}
}
