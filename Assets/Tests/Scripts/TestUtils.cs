﻿using System;

namespace spaceship.test
{
	internal static class TestUtils
	{
		public static void IterateCells(ITileSettings tileSettings, Action<Cell> cellFn)
		{
			long clusterSizeInCells = SpaceUtils.GetClusterSizeInCells(tileSettings);
			for (long i = 0; i < clusterSizeInCells; i++)
			{
				for (long j = 0; j < clusterSizeInCells; j++)
				{
					SpaceCoords coords = new SpaceCoords(i, j, tileSettings);
					Tile tile;
					Cell cell;
					tileSettings.GetTileAndCell(coords, out tile, out cell);
					cellFn(cell);
				}
			}
		}
	}
}
