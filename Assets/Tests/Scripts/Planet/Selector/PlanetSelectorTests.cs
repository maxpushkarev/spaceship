﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace spaceship.test
{
	internal sealed class PlanetSelectorTests
	{
		private const int MIN_RATING = 0;
		private const int MAX_RATING = 20000;

		private IZoomSettings zoomSettings;
		private SceneContext sceneContext;
		private ViewportStub viewport;
		private ITileSettings tileSettings;
		private IPlanetSettings planetSettings;
		private IPlayer player;

		private GameObject selectorGameObject;
		private BruteforcePlanetSelector bruteForcePlanetSelector;
		private AcceleratedPlanetSelector acceleratedPlanetSelector;

		[SetUp]
		public void SetUp()
		{
			GameObject contextGameObject = new GameObject("ContextGameObject");
			sceneContext = contextGameObject.AddComponent<SceneContext>();
			DiContainer container = sceneContext.Container;

			planetSettings = AssetDatabase.LoadAssetAtPath<PlanetSettings>("Assets/Tests/Selector/PlanetSettings.asset");
			tileSettings = AssetDatabase.LoadAssetAtPath<TileSettings>("Assets/Tests/Selector/TileSettings.asset");
			viewport = new ViewportStub();
			zoomSettings = new ZoomSettingsStub();
			player = new PlayerStub();

			TestUtils.IterateCells(tileSettings, c => c.HasPlanet = true);

			container.Bind<IViewport>().FromInstance(viewport).NonLazy();
			container.Bind<IPlayer>().FromInstance(player).NonLazy();
			container.Bind<IZoomSettings>().FromInstance(zoomSettings).NonLazy();
			container.Bind<IPlanetSettings>().FromInstance(planetSettings).NonLazy();
			container.Bind<ITileSettings>().FromInstance(tileSettings).NonLazy();
			container.Inject(planetSettings);
			container.Inject(tileSettings);

			selectorGameObject = new GameObject("test_selector_gameobject");
			bruteForcePlanetSelector = selectorGameObject.AddComponent<BruteforcePlanetSelector>();
			acceleratedPlanetSelector = selectorGameObject.AddComponent<AcceleratedPlanetSelector>();

			container.Inject(bruteForcePlanetSelector);
			container.Inject(acceleratedPlanetSelector);
		}

		[TearDown]
		public void TearDown()
		{
			GameObject.DestroyImmediate(selectorGameObject);
			GameObject.DestroyImmediate(sceneContext.gameObject);
		}


		[Test]
		[Repeat(7)]
		public void ShouldAcceleratedSelectorWorkCorrectly()
		{
			long clusterSizeInCells = SpaceUtils.GetClusterSizeInCells(tileSettings);

			string message = "accelerated selector valid!";
			bool success = true;

			for (int size = zoomSettings.MinScale; (size <= zoomSettings.MaxScale) && success; size++)
			{
				viewport.size = size;

				for (long i = 0; (i < clusterSizeInCells) && success; i++)
				{
					for (long j = 0; (j < clusterSizeInCells) && success; j++)
					{
						viewport.center = new SpaceCoords(i, j, tileSettings);
						List<PlanetEntry> bruteForceList = bruteForcePlanetSelector.SelectPlayerRelevantPlanets(planetSettings.NearestByRatingPlanetCount);
						List<PlanetEntry> acceleratedList = acceleratedPlanetSelector.SelectPlayerRelevantPlanets(planetSettings.NearestByRatingPlanetCount);
						success = CompareLists(bruteForceList, acceleratedList, ref message);
					}
				}

			}

			Assert.IsTrue(success, message);
		}

		private bool CompareLists(
			List<PlanetEntry> bruteForceList, 
			List<PlanetEntry> acceleratedList,
			ref string msg)
		{
			if(bruteForceList.Count != acceleratedList.Count)
			{
				msg = $"Mismatch in list sizes. VP={viewport}; Bruteforce list:{PrintList(bruteForceList)} - Accelerated list: {PrintList(acceleratedList)}";
				return false;
			}

			Dictionary<float, int> bruteforceRating2CountMap = new Dictionary<float, int>();
			Dictionary<float, int> acceleratedRating2CountMap = new Dictionary<float, int>();
			FillMapWithList(bruteforceRating2CountMap, bruteForceList);
			FillMapWithList(acceleratedRating2CountMap, acceleratedList);

			if(bruteforceRating2CountMap.Count != acceleratedRating2CountMap.Count)
			{
				msg = $"Mismatch in map sizes. Player Rate = {player.Rating};{Environment.NewLine}VP={viewport.center.worldCoords};{Environment.NewLine}Bruteforce list:{PrintList(bruteForceList)}{Environment.NewLine}Accelerated list: {PrintList(acceleratedList)}{Environment.NewLine}";
				return false;
			}

			foreach(KeyValuePair<float, int> bruteForcePair in bruteforceRating2CountMap)
			{
				if(!acceleratedRating2CountMap.ContainsKey(bruteForcePair.Key))
				{
					msg = $"Mismatch in list contents. Player Rate = {player.Rating}{Environment.NewLine}VP={viewport}{Environment.NewLine}Bruteforce list:{PrintList(bruteForceList)}{Environment.NewLine}Accelerated list: {PrintList(acceleratedList)}{Environment.NewLine}";
					return false;
				}

				if (acceleratedRating2CountMap[bruteForcePair.Key] != bruteForcePair.Value)
				{
					msg = $"Mismatch in list contents. Player Rate = {player.Rating}{Environment.NewLine}VP={viewport}{Environment.NewLine}Bruteforce list:{PrintList(bruteForceList)}{Environment.NewLine}Accelerated list: {PrintList(acceleratedList)}{Environment.NewLine}";
					return false;
				}
			}

			return true;
		}

		private void FillMapWithList(Dictionary<float, int> map, List<PlanetEntry> list)
		{
			foreach(PlanetEntry pe in list)
			{
				float ratingDiff = pe.planet.ratingDiff;
				if (map.ContainsKey(ratingDiff))
				{
					map[ratingDiff]++;
				}
				else
				{
					map.Add(ratingDiff, 1);
				}
			}
		}

		private string PrintList(List<PlanetEntry> list)
		{
			return $"[{string.Join("; ", list.OrderBy(pe => pe.planet.ratingDiff).Select(pe => pe.planet.ratingDiff))}]";
		}

		private class PlayerStub : IPlayer
		{
			private int rating;
			public PlayerStub()
			{
				this.rating = Random.Range(MIN_RATING, MAX_RATING);
			}

			public float Rating => rating;
		}

		private class ZoomSettingsStub : IZoomSettings
		{
			public int MinScale => 5;
			public int MaxScale => 20;
			public int ModeBorderScale => 0;
		}

		private class ViewportStub : IViewport
		{
			public int size;
			public SpaceCoords center;

			public int Size => size;
			public SpaceCoords Center => center;

			public override string ToString()
			{
				return $"Center = {center} ; size = {size}";
			}
		}
	}
}
