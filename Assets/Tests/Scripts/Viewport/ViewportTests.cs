﻿using NUnit.Framework;
using System;

namespace spaceship.test
{
	internal sealed class ViewportTests
	{
		[Test]
		public void ShouldCalculateViewportCenterCorrectly()
		{
			ITileSettings tileSettings = new SimpleTileSettings();
			SimpleViewport viewport = new SimpleViewport(tileSettings);
			viewport.center = new SpaceCoords(45, 88, tileSettings);
			viewport.size = 8;
			Assert.AreEqual(new SpaceCoords(41, 84, tileSettings), ViewportUtils.GetBottomLeft(viewport, tileSettings));
			viewport.size = 7;
			Assert.AreEqual(new SpaceCoords(42, 85, tileSettings), ViewportUtils.GetBottomLeft(viewport, tileSettings));
		}

		private class SimpleViewport : IViewport
		{
			public readonly ITileSettings tileSettings;
			public SpaceCoords center;
			public int size;

			public SimpleViewport(ITileSettings tileSettings)
			{
				this.tileSettings = tileSettings;
			}

			public SpaceCoords Center => center;
			public int Size => size;
		}

		private class SimpleTileSettings : ITileSettings
		{
			public void GetTileAndCell(SpaceCoords coords, out Tile tile, out Cell cell)
			{
				throw new NotImplementedException();
			}

			public int ClusterSize => 64;
			public int TileSize => 1024;
		}
	}
}
