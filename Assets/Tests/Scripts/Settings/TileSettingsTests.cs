﻿using NUnit.Framework;
using UnityEditor;
using Zenject;

namespace spaceship.test
{
	internal sealed class TileSettingsTests : ZenjectUnitTestFixture
	{
		private TileSettings testTileSettings;
		private SceneContext sceneContext;

		[SetUp]
		public override void Setup()
		{
			base.Setup();
			testTileSettings = AssetDatabase.LoadAssetAtPath<TileSettings>("Assets/Tests/Settings/TileSettings.asset");
			ApplyBindings();
		}

		[Test]
		public void ShouldReturnTileAndCellCorrectly()
		{
			SpaceCoords spaceCoords = new SpaceCoords(0,0,testTileSettings);
			Tile outputTile = null;
			Cell outputCell = null;

			testTileSettings.GetTileAndCell(spaceCoords, out outputTile, out outputCell);
			Assert.AreSame(testTileSettings.ClusterLayout[0], outputTile);
			Assert.AreSame(testTileSettings.ClusterLayout[0].Cells[0], outputCell);

			spaceCoords = new SpaceCoords(5, 2, testTileSettings);
			testTileSettings.GetTileAndCell(spaceCoords, out outputTile, out outputCell);
			Assert.AreSame(testTileSettings.ClusterLayout[1], outputTile);
			Assert.AreSame(testTileSettings.ClusterLayout[1].Cells[9], outputCell);

			spaceCoords = new SpaceCoords(10, 15, testTileSettings);
			testTileSettings.GetTileAndCell(spaceCoords, out outputTile, out outputCell);
			Assert.AreSame(testTileSettings.ClusterLayout[2], outputTile);
			Assert.AreSame(testTileSettings.ClusterLayout[2].Cells[14], outputCell);

			spaceCoords = new SpaceCoords(-2, -12, testTileSettings);
			testTileSettings.GetTileAndCell(spaceCoords, out outputTile, out outputCell);
			Assert.AreSame(testTileSettings.ClusterLayout[3], outputTile);
			Assert.AreSame(testTileSettings.ClusterLayout[3].Cells[2], outputCell);
		}

		[Test]
		public void ShouldCheckFillnessWhenEveryCellHasPlanet()
		{
			TestUtils.IterateCells(testTileSettings, c => c.HasPlanet = true);
			Container.Inject(testTileSettings);
			Assert.DoesNotThrow(() => testTileSettings.ValidatePlanetFillness());
		}

		[Test]
		public void ShouldCheckFillnessWhenHalfOfCellsHasPlanet()
		{
			int counter = 0;
			TestUtils.IterateCells(testTileSettings, c => c.HasPlanet = counter++ % 2 == 0);
			Container.Inject(testTileSettings);
			Assert.DoesNotThrow(() => testTileSettings.ValidatePlanetFillness());
		}

		[Test]
		public void ShouldCheckFillnessWhenNoCellsHasPlanet()
		{
			TestUtils.IterateCells(testTileSettings, c => c.HasPlanet = false);
			Container.Inject(testTileSettings);
			Assert.Throws<TileSettings.NotEnoughPlanetsInCellsException>(() => testTileSettings.ValidatePlanetFillness());
		}

		[Test]
		public void ShouldCheckFillnessWhenHoleInCluster()
		{
			TestUtils.IterateCells(testTileSettings, c => c.HasPlanet = true);
			Tile tile;
			Cell cell;
			SpaceCoords coords = new SpaceCoords(4614, -223, testTileSettings);

			for(int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					testTileSettings.GetTileAndCell(
						new SpaceCoords(coords.worldCoords.x + i, coords.worldCoords.y + j, testTileSettings),
						out tile,
						out cell
					);
					cell.HasPlanet = false;
				}
			}

			Container.Inject(testTileSettings);
			Assert.Throws<TileSettings.NotEnoughPlanetsInCellsException>(() => testTileSettings.ValidatePlanetFillness());
		}

		[Test]
		public void ShouldPlanetDataContainsCorrectCoords()
		{
			TestUtils.IterateCells(testTileSettings, c => c.HasPlanet = true);
			Container.Inject(testTileSettings);

			long clusterSizeInCells = SpaceUtils.GetClusterSizeInCells(testTileSettings);
			for(long i = 0; i<clusterSizeInCells; i++)
			{
				for (long j = 0; j < clusterSizeInCells; j++)
				{
					SpaceCoords coords = new SpaceCoords(i, j, testTileSettings);

					Tile tile;
					Cell cell;
					testTileSettings.GetTileAndCell(coords, out tile, out cell);
					Assert.AreEqual(cell.Planet.clusterPos, coords.worldCoords);
				}
			}
		}

		private void ApplyBindings()
		{
			DiContainer container = Container;

			IZoomSettings zoomSettings = new TestZoomSettings();
			container.BindInstance<IZoomSettings>(zoomSettings);
			container.BindInstance<IPlayer>(new TestPlayer());
			container.BindInstance<ITileSettings>(testTileSettings);

			PlanetSettings testPlanetSettings = AssetDatabase.LoadAssetAtPath<PlanetSettings>("Assets/Tests/Settings/PlanetSettings.asset");
			container.BindInstance<IPlanetSettings>(testPlanetSettings);
		}

		private class TestPlayer : IPlayer
		{
			public float Rating => -1;
		}

		private class TestZoomSettings : IZoomSettings
		{
			public int MinScale => 4;
			public int MaxScale => 8;
			public int ModeBorderScale => -1;
		}

	}

}