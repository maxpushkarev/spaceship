﻿using UnityEditor;

namespace spaceship.editor
{
	[CustomPropertyDrawer(typeof(ViewportUIStatesDescription))]
	internal sealed class ViewportUIStatesDescriptionPropertyDrawer : BaseStatesDescriptionPropertyDrawer<ZoomStates, ViewportUIStateBehaviour, ViewportUIStateConfiguration>
	{
	}
}
