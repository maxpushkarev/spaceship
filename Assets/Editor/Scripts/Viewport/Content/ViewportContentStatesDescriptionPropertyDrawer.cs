﻿using UnityEditor;

namespace spaceship.editor
{
	[CustomPropertyDrawer(typeof(ViewportContentStatesDescription))]
	internal sealed class ViewportContentStatesDescriptionPropertyDrawer
		: BaseStatesDescriptionPropertyDrawer<ZoomStates, ViewportContentStateBehaviour, ViewportContentStateConfiguration>
	{
	}
}
