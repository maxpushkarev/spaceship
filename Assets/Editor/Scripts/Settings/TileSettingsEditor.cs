﻿using System.IO;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace spaceship.editor
{
	[CustomEditor(typeof(TileSettings))]
	internal sealed class TileSettingsEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			TileSettings tileSettings = target as TileSettings;

			if(GUILayout.Button("Create empty tiles"))
			{
				tileSettings.BuildEmptyTiles();
				return;
			}

			if (GUILayout.Button("Generate random tile cluster layout"))
			{
				tileSettings.GenerateRandomTileClusterLayout();
				return;
			}

			if (GUILayout.Button("Generate empty cells"))
			{
				tileSettings.GenerateEmptyCells();
				return;
			}

			if (GUILayout.Button("Fill cells with planets"))
			{
				InitDependencies(tileSettings);
				tileSettings.FillCellsWithPlanets();
				FinalizeDependencies();
				return;
			}

			if (GUILayout.Button("Validate planet fillness"))
			{
				InitDependencies(tileSettings);
				tileSettings.ValidatePlanetFillness();
				FinalizeDependencies();
				return;
			}

			if (GUILayout.Button("Generate decorations"))
			{
				InitDependencies(tileSettings);
				tileSettings.GenerateDecorations();
				FinalizeDependencies();
				return;
			} 
		}

		private void InitDependencies(TileSettings tileSettings)
		{
			string tileSettingsDirectory = Path.GetDirectoryName(
				AssetDatabase.GetAssetPath(tileSettings)
			);

			DecorationSettings decorationSettings = AssetDatabase.LoadAssetAtPath<DecorationSettings>(
				Path.Combine(tileSettingsDirectory, "DecorationSettings.asset")
			);

			PlanetSettings planetSettings = AssetDatabase.LoadAssetAtPath<PlanetSettings>(
				Path.Combine(tileSettingsDirectory, "PlanetSettings.asset")
			);

			ZoomSettings zoomSettings = AssetDatabase.LoadAssetAtPath<ZoomSettings>(
				Path.Combine(tileSettingsDirectory, "ZoomSettings.asset")
			);

			DiContainer container = new DiContainer(StaticContext.Container);
			container.BindInstance<IPlayer>(new PlayerStub());
			container.BindInstance<ITileSettings>(tileSettings);
			container.BindInstance<IZoomSettings>(zoomSettings);
			container.Bind<DecorationSettings>().FromInstance(decorationSettings);
			container.BindInstance<IPlanetSettings>(planetSettings);
			container.Inject(tileSettings);
		}

		private void FinalizeDependencies()
		{
			StaticContext.Clear();
		}

		private class PlayerStub : IPlayer
		{
			public float Rating => 0;
		}

	}
}
