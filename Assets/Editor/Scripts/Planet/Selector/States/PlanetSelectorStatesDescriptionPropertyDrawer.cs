﻿using UnityEditor;

namespace spaceship.editor
{
	[CustomPropertyDrawer(typeof(PlanetSelectorStatesDescription))]
	internal sealed class PlanetSelectorStatesDescriptionPropertyDrawer : BaseStatesDescriptionPropertyDrawer<ZoomStates, PlanetSelectorStateBehaviour, PlanetSelectorStateConfiguration>
	{
	}
}
