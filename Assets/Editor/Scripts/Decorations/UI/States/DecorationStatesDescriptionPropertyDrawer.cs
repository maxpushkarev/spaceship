﻿using UnityEditor;

namespace spaceship.editor
{
	[CustomPropertyDrawer(typeof(DecorationUIStatesDescription))]
	internal sealed class DecorationStatesDescriptionPropertyDrawer : BaseStatesDescriptionPropertyDrawer<DecorationStates, DecorationUIStateBehaviour, DecorationUIStateConfiguration>
	{
	}
}
