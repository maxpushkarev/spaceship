﻿using System;
using UnityEditor;
using UnityEngine;

namespace spaceship.editor
{
	internal abstract class BaseStatesDescriptionPropertyDrawer<TState, TBaseStateBehavior, TStateConfiguration> : PropertyDrawer
		where TState : struct
		where TBaseStateBehavior : BaseStateBehaviour
		where TStateConfiguration : StateConfiguration<TBaseStateBehavior>
	{
		private const string PROP_NAME = "stateConfigurations";

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
			SerializedProperty values = property.FindPropertyRelative(PROP_NAME);
			int statesSount = StatesDescription<TState, TBaseStateBehavior, TStateConfiguration>.GetStatesCount();
			int indentLevel = EditorGUI.indentLevel;
			EditorGUI.indentLevel++;

			position.y += EditorGUI.GetPropertyHeight(property, false);
			for (int i = 0; i < statesSount; i++)
			{
				SerializedProperty elementProp = values.GetArrayElementAtIndex(i);
				EditorGUI.PropertyField(position, elementProp, new GUIContent(Enum.GetNames(typeof(TState))[i]), true);
				position.y += EditorGUI.GetPropertyHeight(elementProp, true);
			}

			EditorGUI.indentLevel = indentLevel;
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			SerializedProperty values = property.FindPropertyRelative(PROP_NAME);
			int statesCount = StatesDescription<TState, TBaseStateBehavior, TStateConfiguration>.GetStatesCount();
			float val = EditorGUI.GetPropertyHeight(property, false);

			for (int i = 0; i < statesCount; i++)
			{
				SerializedProperty elementProp = values.GetArrayElementAtIndex(i);
				val += EditorGUI.GetPropertyHeight(elementProp, true);
			}
			return val;
		}
	}
}
